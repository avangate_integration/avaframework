
/**
 * Layout 7
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 */
AvaCart.Layouts.layout7 = {
    settings: {},
    render: function () {
        $('body').addClass('layout-7');
        try {
            $('.tmp-li-fix').removeClass('tmp-li-fix');
            $('#order__container').addClass('col-md-12');
            if (avaPage.current === 'checkout') {
                $('#header > .container > .row').removeClass('row');
                
                $('li#order__cart__contents').addClass('col-md-12');
                $('li#order__cart__contents').addClass('col-md-12');
                $('#order__checkout__billing__data').addClass('col-lg-6');
                $('#order__checkout__payoptions__data').addClass('col-lg-6');
                $('#order__additional__fields').addClass('col-md-12');
                $('.order__checkout__button__container').parent('li').addClass('order__button__container col-md-6');
                $('.order__vat__note.products-content__vat').parents('tr:first').addClass('vat-row');
                
                $('<li class="order__checkout__bottom__content__wrapper clearfix"><ul class="order__checkout__bottom__content"></ul></li>').appendTo($('.order__checkout'));
                $('.order__checkout__bottom__content').append($('.order__button__container'));
                
                $('#order__checkout__footer').removeClass('row');
                $('.assistance-content__left').removeClass('col-md-8').addClass('col-md-7');
                $('.assistance-content__right').addClass('col-md-offset-1');
                
                $('#order__finalTotalPrice').insertBefore($('.order__checkout__button__container'));
            } else if (avaPage.current == 'verify') {
                $('#frmFinish > div').addClass('col-md-12');
            }

            $('#order__finish__finish__order').addClass('col-md-12');

        } catch(error) {
            avaLog(error);
        }
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
};