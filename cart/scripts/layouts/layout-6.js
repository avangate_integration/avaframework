
/**
 * Layout 6
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 */
AvaCart.Layouts.layout6 = {
    settings: {},
    render: function () {
        $('body').addClass('layout-6');
        try {
            $('.tmp-li-fix').removeClass('tmp-li-fix');
            $('#order__container').addClass('col-md-12');
            if (avaPage.current === 'checkout') {
                $('#header > .container > .row').removeClass('row');
                
                $('li#order__cart__contents').addClass('col-md-12');
                $('li#order__cart__contents').addClass('col-md-12');
                
                // Billing and payoptions wrapper
                $('#order__cart__contents').after('<li class="order__checkout__billing__and__payoptions col-md-12"><div class="order__checkout__billing__and__payoptions__header clearfix"></div><ul class="order__checkout__billing__and__payoptions__content clearfix"></ul></li>');
                $('#order__checkout__billing__data').appendTo('.order__checkout__billing__and__payoptions__content');
                $('#order__checkout__billing__data').addClass('col-lg-6');
                $('#order__checkout__payoptions__data').appendTo('.order__checkout__billing__and__payoptions__content');
                $('#order__checkout__payoptions__data').addClass('col-lg-6');
                
                $('#order__additional__fields').addClass('col-md-12');
                $('.order__checkout__button__container').parent('li').addClass('order__button__container').appendTo($('#order__checkout__payoptions__data .payment__content'));
                $('.order__vat__note.products-content__vat').parents('tr:first').addClass('vat-row');
                
                $('#order__checkout__footer').removeClass('row');
                $('.assistance-content__left').removeClass('col-md-8').addClass('col-md-7');
                $('.assistance-content__right').addClass('col-md-offset-1');
                
                $('#order__finalTotalPrice').insertBefore($('.order__checkout__button__container'));
            } else if (avaPage.current == 'verify') {
                $('#frmFinish > div').addClass('col-md-12');
            }

            $('#order__finish__finish__order').addClass('col-md-12');

        } catch(error) {
            avaLog(error);
        }
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(true, this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
};