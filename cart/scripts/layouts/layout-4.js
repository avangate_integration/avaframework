
/**
 * Layout 4
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 */
AvaCart.Layouts.layout4 = {
    settings: {},
    render: function () {
        $('body').addClass('layout-4');
        try {
            $('.tmp-li-fix').removeClass('tmp-li-fix');
            $('#order__container').addClass('col-md-12');
            if (avaPage.current == 'checkout') {
                $('<li class="col-md-4 column-1-wrapper"></li>').append($('<ul class="column-1"></ul>').append($('#order__cart__contents'))).appendTo($('.order__checkout'));
                
                if (avaPage.isExpressCheckout) {
                    $('<li class="col-md-4 column-2-wrapper"></li>').append($('<ul class="column-2"></ul>').append($('#order__checkout__payoptions__data'))).appendTo($('.order__checkout'));

                    $('<li class="col-md-4 column-3-wrapper"></li>').append($('<ul class="column-3"></ul>').append($('#order__checkout__billing__data')).append($('#order__additional__fields')).append($('.order__checkout__button__container').parent('li'))).appendTo($('.order__checkout'));
                } else {
                    $('<li class="col-md-4 column-2-wrapper"></li>').append($('<ul class="column-2"></ul>').append($('#order__checkout__billing__data'))).appendTo($('.order__checkout'));

                    $('<li class="col-md-4 column-3-wrapper"></li>').append($('<ul class="column-3"></ul>').append($('#order__checkout__payoptions__data')).append($('#order__additional__fields')).append($('.order__checkout__button__container').parent('li'))).appendTo($('.order__checkout'));
                }

                $('#order__finalTotalPrice').insertBefore($('.order__checkout__button__container'));
                
                $('#samedelivery[type=hidden]').parent('td').parent('tr').hide();
            } else if (avaPage.current == 'verify') {
                $('#frmFinish > div').addClass('col-md-12');
            }
            
            $('#order__finish__finish__order').addClass('col-md-12');
        } catch(error) {
            avaLog(error);
        }
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
