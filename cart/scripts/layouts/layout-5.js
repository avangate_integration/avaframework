
/**
 * Layout 5
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 */
AvaCart.Layouts.layout5 = {
    settings: {},
    render: function () {
        $('body').addClass('layout-5');
        try {
            $('.tmp-li-fix').removeClass('tmp-li-fix');
            $('#order__container').addClass('col-md-12');
            if (avaPage.current == 'checkout') {
                $('li#order__cart__contents').addClass('col-md-12');
//                $('li#order__cart__contents').after('col-md-12');
                
                if (avaPage.isExpressCheckout) {
                    $('<li class="col-md-6 right-wrapper-container"></li>').append($('<ul class="right-wrapper"></ul>').append($('#order__checkout__billing__data')).append($('.order__checkout__button__container').parent('li'))).insertAfter($('li#order__cart__contents'));
                    $('<li class="col-md-6 left-wrapper-container"></li>').append($('<ul class="left-wrapper"></ul>').append($('#order__checkout__payoptions__data'))).insertAfter($('li#order__cart__contents'));
                } else {
                    $('<li class="col-md-6 right-wrapper-container"></li>').append($('<ul class="right-wrapper"></ul>').append($('#order__checkout__payoptions__data')).append($('.order__checkout__button__container').parent('li'))).insertAfter($('li#order__cart__contents'));
                    $('<li class="col-md-6 left-wrapper-container"></li>').append($('<ul class="left-wrapper"></ul>').append($('#order__checkout__billing__data'))).insertAfter($('li#order__cart__contents'));
                }

                    $('#order__additional__fields').addClass('col-md-12');
                    $('.order__checkout__button__container').parent('li').addClass('col-md-12');

                    $('#order__finalTotalPrice').insertBefore($('.order__checkout__button__container'));
            } else if (avaPage.current == 'verify') {
                $('#frmFinish > div').addClass('col-md-12');
            }
            
            $('#order__finish__finish__order').addClass('col-md-12');
        } catch(error) {
            avaLog(error);
        }
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
};