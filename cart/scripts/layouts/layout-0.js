/**
 * Layout 1
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 */
AvaCart.Layouts.layout0 = {
    settings: {},
    render: function () {
        $('body').addClass('layout-0');
        try {
            $('.tmp-li-fix').removeClass('tmp-li-fix');
            $('#order__container').addClass('col-md-12');
            if (avaPage.isCheckoutPage) {
                $('#order__checkout__billing__data, li#order__cart__contents, #order__checkout__payoptions__data, #order__additional__fields').addClass('col-md-12');
                $('.order__checkout__button__container').parent('li').addClass('col-md-12');

                $('#order__finalTotalPrice').insertBefore($('.order__checkout__button__container'));
            }
            if (avaPage.isCheckoutPage && !avaPage.isExpressCheckout) {
                $('table.form-table-billing-payment').append('<tbody id="autorenewal-container-wrapper"><tr class="autorenewal-container"><td class="order__checkout__form__label">&nbsp;</td><td class="order__checkout__form__input"></td></tr></tbody><tbody id="cta-submit-button-container-wrapper"><tr class="cta-submit-button-container"><td class="order__checkout__form__label">&nbsp;</td><td class="order__checkout__form__input"></td></tr></tbody>');
                $('.order__checkout__button__container').parent('li').hide();
                $('#cta-submit-button-container-wrapper .order__checkout__form__input').append($('.order__checkout__button__container'));
                $('#order__finalTotalPrice').insertBefore($('.order__checkout__button__container'));
                $('#autorenewal-container-wrapper .order__checkout__form__input').append($('#order__autorenewal__container'));
            } 
            if (avaPage.isVerifyPage) {
                $('#frmFinish > div').addClass('col-md-12');
            } else if (avaPage.isPartnersPage) {
                $('.signup_container').addClass('col-md-12');
            }

            $('#order__finish__finish__order').addClass('col-md-12');
        } catch(error) {
            avaLog(error);
        }
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
