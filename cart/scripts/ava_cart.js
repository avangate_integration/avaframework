
/**
 * AvaCart
 * 
 * @namespace AvaCart
 * @version  2.0.0
 * @author Adrian Staniloiu <adrian.staniloiu@avangate.com>
 * @type {Object} 
 */
var AvaCart = {
	version: 2.0,
	/** 
	 * Utilities and extra methods or help functions.
	 * 
	 * @namespace Utils  
	 * @memberof AvaCart
	 */
	Utils: {},
	/** 
	 * All functions that are global scoped. 
	 * 
	 * @namespace Functions  
	 * @memberof AvaCart
	 */
	Functions: {},
	/** 
	 * 
	 * @namespace Layouts  
	 * @memberof AvaCart
	 */
	Layouts: {},
	/** 
	 * 
	 * @namespace Widgets  
	 * @memberof AvaCart
	 * 
	 */
	Widgets: {},
	/** 
	 * 
	 * @namespace Callbacks  
	 * @memberof AvaCart
	 */
	Callbacks: {},
	/** 
	 * Use this namespace for all custom widgets and modifications.
	 * 
	 * @namespace Custom  
	 * @memberof AvaCart
	 */
    Custom: {},
	/** 
	 * Use this namespace for all scoped data.
	 * 
	 * @namespace Data  
	 * @memberof AvaCart
	 */
	Data: {},
	/** 
	 * Use this namespace for all patches.
	 * 
	 * @namespace Patches  
	 * @memberof AvaCart
	 */
	Patches: {
		Functions: {},
		initOrder: [],
		init: function () {
			try {
				for (var i in this.initOrder) {
					if (this.Functions.hasOwnProperty(this.initOrder[i])) {
						try {
							this.Functions[this.initOrder[i]]();
						} catch (error) {
							avaLog(error);
						}
					}
				}
			} catch (error) {
				avaLog(error);
			}
		}
	}
}

/**
 * Initialize
 *
 * Will iterate the callbacks object and try to callback fn
 * if condition is true or undefined;
 *
 * @author Adrian Staniloiu <adrian.staniloiu@avangate.com>
 * @version 1.0.0
 * @since   1.0.0
 * @example <caption>Will execute the callbacks strings in the array in the added order.</caption>
 * AvaCart.initialize(['callback1', 'callback2']);
 */
AvaCart.initialize = function (args) {
	if ((typeof args == 'undefined' || !(args instanceof Array)) && !window.AvaCartInitialized) {
		return false;
	}
	for (var i in args) {
		try {
			if (typeof this.Callbacks[args[i]] != 'undefined' && this.Callbacks[args[i]] instanceof Object) {
				if ((typeof this.Callbacks[args[i]].condition == 'undefined' || (typeof this.Callbacks[args[i]].condition != 'undefined' &&
					this.Callbacks[args[i]].condition === true)) && typeof this.Callbacks[args[i]].callback == 'function') {
					try {
						this.Callbacks[args[i]].callback();
					} catch (error) {
						avaLog(error);
					}
				}
			}
		} catch (error) {
			avaLog(error);
		}
	}

	window.AvaCartInitialized = true;
};
/**
 * @method AvaCart.Functions.getTranslation
 * @param  {String} __phrase - The phrase that will be searched in the dictionary.
 * @param  {String} __def - Default value.
 * @param  {String} __lang - Language, if false will be get form omniture_vars.
 * @todo Add references to template dictionary. 
 * 
 * @return {String}
 */
AvaCart.Functions.getTranslation = function (__phrase, __def, __lang) {
	try {
		var omnitureLANG = (typeof omniture_vars !== 'undefined' && omniture_vars.hasOwnProperty('LANGUAGE')) ? omniture_vars.LANGUAGE.replace('-', '_') : '';
		if (typeof __lang !== 'undefined' && typeof __lang !== null)
			omnitureLANG = __lang.replace('-', '_');

		if (templateDictionary.hasOwnProperty(omnitureLANG) && templateDictionary[omnitureLANG].hasOwnProperty(__phrase)) {
			return templateDictionary[omnitureLANG][__phrase];
		} else {
			if (typeof __def !== 'undefined' && typeof __def !== null && __def != "")
				return __def;
			else if (templateDictionary.hasOwnProperty('en') && templateDictionary['en'].hasOwnProperty(__phrase) && (typeof __lang === 'undefined' || typeof __lang === null))
				return templateDictionary['en'][__phrase];
			else if ((typeof __lang !== 'undefined' || typeof __lang !== null) && templateDictionary.hasOwnProperty(__lang) && templateDictionary[__lang].hasOwnProperty(__phrase))
				return templateDictionary[__lang][__phrase];
			else
				return '';
		}
	} catch (error) {
		avaLog(error);
	}
};

// Fix Chrome $.cookie Bug
if (typeof $ != 'undefined' && typeof $.cookie != 'undefined') {
    $.cookie.raw = true;
}

// Patches
try {
    AvaCart.Patches.Functions.boletoBancarioExpressFlow = function () {
        try {
            if (avaPage && avaPage.isExpressCheckout) {
				$('#order__checkout__billing__payoptions__table > tbody').show();
				$('#payment_radios_wrap_tr').hide();
				$('#order__checkout__form__billing__currency').after($('#boleto_fiscal_code_fields'));
            }
        } catch (error) {
            avaLog(error);
        }
    };
	AvaCart.Patches.Functions.iyzico = function() {
		// Turkey
		if ($('#billingcountry').val() === '213') {
			$('#card__data__nr').after($('#data__installments'));
		}
	};
	AvaCart.Patches.initOrder.push('boletoBancarioExpressFlow', 'iyzico');
    $(window).load(function() {
		AvaCart.Patches.init();
	});
} catch (error) {
    avaLog(error);
}