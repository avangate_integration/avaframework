/**
 * Up Selling
 * @memberof AvaCart.Widgets
 * @namespace upSelling
 * @version 1.0.0
 * @since   1.0.0 
 * @example <caption>Render the upsell</caption>
 *
        upSell.callbacks.customizeUpSell = function() {
            AvaCart.Widgets.upSelling.initialize({
                showPriceBeforeDescription: true,
                hidePriceLabel: true
            });
        };
 * 
 * @author  Alexandru Salajan (alexandru.salajan@avangate.com)
 * 
 */
AvaCart.Widgets.upSelling = {
    /** 
     * Default Wdiget settings. This will be merged with the widget initialize options object.
     *
     * @member {Object} 
     *
     * @property {boolean} showPriceBeforeDescription 
     * @property {boolean} hidePriceLabel 
     */
    settings: {
        showPriceBeforeDescription: false,
        hidePriceLabel: false,
    },
    functions: {},
    render: function () {
        if (this.settings.showPriceBeforeDescription) {
            $('#order__upsell__custom__description').insertAfter('.order__upsell__new__price__total__wrap');
        }
        if (this.settings.hidePriceLabel) {
            $('.order__upsell__new__price__total__label').hide();
        }
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    /**
    * @method
    * @param {object} [options=AvaCart.Widgets.upSelling.settings] - Check this.settings for default options.
    */
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}