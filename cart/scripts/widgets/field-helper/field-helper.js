/**
 * Field Helper
 *
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaCart.Widgets.fieldHelper = {
/* ==========================================================================
   SETTINGS
   ========================================================================== */
    settings: {
        placeholder     : null,
        
        iconClass       : 'icon-info-circled',
        contentTypeHtml : false,
        contentImageSrc : null, // for contentTypeHtml = false
        contentHtml     : null, // for contentTypeHtml = true
        wrapContent     : true,
        mirrored        : false,
        
        widgetClass     : ''
    },
    template: null,

/* ==========================================================================
   FUNCTIONS
   ========================================================================== */
    functions: {},
/* ==========================================================================
   RENDER TEMPLATE
   ========================================================================== */
    render: function () {
    /* ===================
       Template context
       =================== */
        var context = {
            settings: this.settings
        }
        
        if (this.settings.wrapContent) {
            $(this.settings.placeholder).wrap('<div class="field-wrapper ' + this.settings.widgetClass + '"></div>');
        } else {
            $(this.settings.placeholder).addClass(this.settings.widgetClass);
        }
        
        var html = nunjucks.renderString(this.template, context);
        $(this.settings.placeholder).after(html);
    },

/* ==========================================================================
   EVENTS
   ========================================================================== */
    events: function () {},
/* ==========================================================================
   DESTROY
   ========================================================================== */
    destroy: function () {},
/* ==========================================================================
   BEFORE
   ========================================================================== */
    before: function () {},
/* ==========================================================================
   CALLBACK
   ========================================================================== */
    callback: function () {},

/* ==========================================================================
   INITIALIZATION
   ========================================================================== */
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}