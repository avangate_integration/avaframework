/**
 * Select Boxes
 * 
 * @memberof AvaCart.Widgets
 * @namespace selectBoxes
 * 
 * @author Adrian Staniloiu <adrian.staniloiu@avangate.com>
 * @version 1.2.0
 * @since   1.0.0 
 */
AvaCart.Widgets.selectBoxes = {
    /** 
     * Default Wdiget settings. This will be merged with the widget initialize options object.
     *
    * @member {Object} 
    * @property {string} defaultWidth
    * @property {string} selector
    * @property {string} showArrowIcon
    * @property {string} arrowIcon
    * @property {string} focusArrowIcon
    */
    settings: {
        defaultWidth: '205px',
        selector: 'select:not(#Promotion_Methods)',
        showArrowIcon: false,
        arrowIcon: 'icon-down-open',
        focusArrowIcon: 'icon-up-open'
    },
    /**
     * @version 1.0.0
     * @since   1.0.0
     * @param {settings} cc
     */
    resetSelectBoxesSelectedValues: function (settings) {
        self = this;
        $('.select-box').each(function () {
            var $this = $(this);
            $this.removeClass('order__select__focus').find('.selected-option').html($this.find('option:selected').text());
            if (settings.showArrowIcon) {
                $this.find('.select-box-arrow').removeClass(settings.focusArrowIcon).addClass(settings.arrowIcon);
            }
            if ($this.find('select').val() == '') {
                $this.removeClass('valid-select');
                $this.find('.selected-option').addClass('placeholder-color');
            } else {
                $this.find('.selected-option').removeClass('placeholder-color');
            }
        });
    },
    render: function () {
        var self = this;
        $(this.settings.selector).each(function () {
            var $this = $(this),
                id = ($this.attr('id') ? $this.attr('id') : avaSlugify($this.attr('name'))).replace(/\[/g, '').replace(/\]/g, '') + '-select-box';
            value = $.trim($this.find('option:selected').text());
            style = 'width:' + (this.offsetWidth ? this.offsetWidth + 'px' : self.settings.defaultWidth);
            $this.addClass('force-reset-select').wrap('<div class="select-box" style="' + style + '" id="' + id + '" />');
            $('#' + id).prepend('<span class="selected-option" ' + ($this.val() == '' ? 'placeholder-color' : '') + '>' + value + '</span>');
            if (self.settings.showArrowIcon) {
                $('#' + id).prepend('<div class="select-box-arrow-wrapper"><span class="select-box-arrow icon ' + self.settings.arrowIcon + '"></span></div>');
            } else {
                $('#' + id).prepend('<div class="select-box-arrow-wrapper"><span class="select-box-arrow"></span></div>');
            }
        });
    },
    events: function () {
        var self = this;
        $(self.settings.selector).each(function () {
            var $this = $(this);
            if ($this.hasClass('order__select__field__error')) {
                $this.parent().addClass('order__select__field__error');
            } else {
                $this.parent().removeClass('order__select__field__error');
            }
            $this.focus(function () {
                $(this).parent().addClass('order__select__focus');
                if (self.settings.showArrowIcon) {
                    $(this).parent().find('.select-box-arrow').removeClass(self.settings.arrowIcon).addClass(self.settings.focusArrowIcon);
                }
                $(this).parent().removeClass('order__select__field__error');
            }).blur(function () {
                var $this = $(this);
                $this.parent().removeClass('order__select__focus');
                if (self.settings.showArrowIcon) {
                    $this.parent().find('.select-box-arrow').removeClass(self.settings.focusArrowIcon).addClass(self.settings.arrowIcon);
                }
                if ($this.hasClass('order__select__field__error')) {
                    $this.parent().addClass('order__select__field__error');
                } else {
                    $this.parent().removeClass('order__select__field__error');
                }
            }).change(function () {
                var $this = $(this);
                $this.parent().find('.selected-option').html($this.find('option:selected').text());
                $this.parent().removeClass('order__select__focus');
                if (self.settings.showArrowIcon) {
                    $this.parent().find('.select-box-arrow').removeClass(self.settings.focusArrowIcon).addClass(self.settings.arrowIcon);
                }
                self.resetSelectBoxesSelectedValues(self.settings);
            }).bind('keyup', function () {
                var $this = $(this);
                $this.parent().find('.selected-option').html($this.find('option:selected').text());
                $this.parent().removeClass('order__select__focus');
                if (self.settings.showArrowIcon) {
                    $this.parent().find('.select-box-arrow').removeClass(self.settings.focusArrowIcon).addClass(self.settings.arrowIcon);
                }
                self.resetSelectBoxesSelectedValues(self.settings);
            });
        });
        $('#cbExpMounth, #cbExpYear').each(function (index, elem) {
            $(this).focus(function () {
                var container = $(this).parents('#card__data__expdates');
                container.find('.order__checkout__form__label__error').removeClass('order__checkout__form__label__error');
                container.find('.order__text__label__error').removeClass('order__text__label__error');
            });
        });

        $('.order__select__focus').click(function (event) {
            $(this).removeClass('order__select__focus');
            if (self.settings.showArrowIcon) {
                $(this).find('.select-box-arrow').removeClass(self.settings.focusArrowIcon).addClass(self.settings.arrowIcon);
            }
        });
        // For Checkout Without Review add errors after validating ajax.
        if (avaPage.isCheckoutPage) {
            $(document).ajaxSuccess(function () {
                $(self.settings.selector).each(function () {
                    var $this = $(this);
                    if ($this.hasClass('order__select__field__error')) {
                        $this.parent().addClass('order__select__field__error');
                    } else {
                        $this.parent().removeClass('order__select__field__error');
                    }
                    self.resetSelectBoxesSelectedValues(self.settings);
                });
            })
        }
    },
    destroy: function () {
        $('.select-box').each(function () {
            var $this = $(this);
            $this.after($this.find('select').removeClass('force-reset-select'));
            $this.remove();
        });
    },
    before: function () { },
    callback: function () { },
    /**
    * @method
    * @param {object} [options=AvaCart.Widgets.selectBoxes.settings] - Check this.settings for default options.
    */
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}