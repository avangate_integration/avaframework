/**
 * Cart Steps
 * 
 * @memberof AvaCart.Widgets
 * @namespace cartSteps
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaCart.Widgets.cartSteps = {
    /** 
     * Default Wdiget settings. This will be merged with the widget initialize options object.
     *
     * @member {Object} 
     * @property {string} placeholder The placeholder class or id e.g. ".cart-steps", created before the initialize.
     */
    settings: {
        placeholder: null,
        steps: null,
        checkedIconClass: 'icon-check',
        noOfSteps: 4,
        startFromStep: false,
        removeSteps: false,
        showOnPages: false,
        showLinks: false,
        showStepBox: false,
        showStepIcon: true,
        colorCheckedSteps: false,
        colorCurrentStep: false,
        currentStep: 1,
        showLabelsOnTop: true,
        barStyle: 'solid',
        layout: '',
        widgetClass: null
    },
    /** 
     * The HTML content that will parsed by nunjucks. This should be mapped to "AvaCart.Widgets.cartSteps" namespace from "gulpfile.js"
     * @member {string} 
     */
    template: null,
    /** 
     * Helper Functions.
     * @namespace AvaCart.Widgets.cartSteps.functions 
     * @memberof AvaCart.Widgets.cartSteps
     */
    functions: {
        /**
         * @version 1.0.0
         * @since   1.0.0
         * @param {object} settings
         * @returns {string}
         */
        initialSetup: function (settings) {
            // show on pages
            if (settings.showOnPages && $.inArray(avaPage.current, settings.showOnPages) == -1) {
                return false;
            }
            
            // start from step
            if (settings.startFromStep && settings.startFromStep <= settings.noOfSteps) {
                for (i in settings.steps) {
                    if (i < settings.startFromStep) {
                        delete settings.steps[i];
                        --settings.noOfSteps;
                    }
                }
            }

            // if without review -> remove step 3
            if (typeof omniture_vars !== undefined) {
                if (omniture_vars.CART_TYPE == 2) {
                    delete settings.steps[3];
                    --settings.noOfSteps;
                }
            }

            // remove steps
            if (settings.removeSteps) {
                for (i in settings.steps) {
                    if ($.inArray(i, settings.removeSteps) > -1) {
                        delete settings.steps[i];
                        --settings.noOfSteps;
                    }
                }
            }

            // set current step
            if (typeof avaPage !== undefined) {
                settings.currentStep = 1;
                for (i in settings.steps) {
                    if (settings.steps[i].page == avaPage.current) {
                        break;
                    }
                    settings.currentStep++;
                }
            }

            return true;
        }
    },
    render: function () {
        if (!this.functions.initialSetup(this.settings)) return false;
        var context = {
            settings: this.settings
        }

        var html = nunjucks.renderString(this.template, context);
        $(this.settings.placeholder).append(html);
    },
    events: function () { },
    destroy: function () { },
    before: function () { },
    callback: function () { },
    initialize: function (options) {
        if (typeof __order_steps == 'undefined') return false;
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}