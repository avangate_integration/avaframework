/**
 * Input Wrappers
 *
 * @author Alexandru Salajan (alexandru.salajan@avangate.com)
 */
AvaCart.Widgets.inputWrappers = {
    settings: {
        inputSelector: null,
        widgetClass: ''
    },
    template: null,
    functions: {
        inputsWithTooltips: function (settings) {
//            $('#order__checkout__billing__payoptions__table .order__text__field + .tooltip').each(function() {
//                var inputContainer = $(this).parent();
//                var inputField = inputContainer.children('.order__text__field');
//                var inputTooltip = inputContainer.children('.tooltip');
//                $('<div class="field-wrapper"></div>').append(inputField).append(inputTooltip).prependTo(inputContainer);
//            });
            
            $(settings.inputSelector + ' + .tooltip').each(function() {
                var inputContainer = $(this).parent();
                var inputField = inputContainer.children(settings.inputSelector);
                var inputTooltip = inputContainer.children('.tooltip');
                
                $('<div class="field-wrapper ' + settings.widgetClass + '"></div>').append(inputField).append(inputTooltip).prependTo(inputContainer);
                inputTooltip.hide();
                
                AvaCart.Widgets.fieldHelper.initialize({
                    placeholder: inputTooltip,
                    iconClass: 'icon-info-circled',
                    contentTypeHtml: true,
                    wrapContent: false,
                    mirrored: false,
                    contentHtml: inputTooltip.find('.tooltip_content').html()
                });
            });
//            
//            $('textarea + .tooltip').each(function() {
//                var inputContainer = $(this).parent();
//                var inputField = inputContainer.children('textarea');
//                var inputTooltip = inputContainer.children('.tooltip');
//                $('<div class="field-wrapper"></div>').append(inputField).append(inputTooltip).prependTo(inputContainer);
//            });
//            
//            $('select + .tooltip').each(function() {
//                var inputContainer = $(this).parent();
//                var inputField = inputContainer.children('select');
//                var inputTooltip = inputContainer.children('.tooltip');
//                $('<div class="field-wrapper"></div>').append(inputField).append(inputTooltip).prependTo(inputContainer);
//            });
//            
//            $('.select-box + .tooltip').each(function() {
//                var inputContainer = $(this).parent();
//                var inputField = inputContainer.children('.select-box');
//                var inputTooltip = inputContainer.children('.tooltip');
//                $('<div class="field-wrapper"></div>').append(inputField).append(inputTooltip).prependTo(inputContainer);
//            });
        }
    },
    render: function () {
        this.functions.inputsWithTooltips(this.settings);
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
