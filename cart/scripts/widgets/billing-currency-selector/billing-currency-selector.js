/**
 * Billing Currency Selector
 * @description Appends the billing currency selector into the specified placeholder.
 * @author Alexandru Salajan (alexandru.salajan@avangate.com)
 */
AvaCart.Widgets.billingCurrencySelector = {
    settings: {
        placeholder:        null,
        showLabel:          true,
                
        widgetClass:        null
    },
    template: null,
    render: function () {
        $('#order__checkout__form__billing__currency').appendTo($(this.settings.placeholder));
        
//        $('#order__checkout__form__billing__currency').clone(true)
//            .removeAttr('id')
//            .removeAttr('class')
//            .addClass(this.settings.widgetClass);
//            .appendTo($(this.settings.placeholder));
//        $(this.settings.placeholder).find('*').removeAttr('id');
//        $(this.settings.placeholder).find('select').change(function (event) {
//            $('select#billing_currency').val($(this).val());
//        });
        
        // LABEL
        if (!this.settings.showLabel) {
            $(this.settings.placeholder).find('.order__checkout__form__label').hide();
        }
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}