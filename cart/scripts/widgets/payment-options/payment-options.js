/**
 * Payment Options
 * @memberof AvaCart.Widgets
 * @namespace paymentOptions
 * @version 1.5.1
 * @since   1.0.0 
 * @example <caption>Render the payment options</caption>
 *         AvaCart.Widgets.paymentOptions.initialize({
            placeholder: '.payment-options-wrapper', // created before the initialization "AvaCart.Widgets.paymentOptions.before"
            autoDetectIconsPlaceholder: '.payment-options-card-icons',

            // spriteSrc: 'https://secure.avangate.com/images/merchant/6531b32f8d02fece98ff36a64a7c8260/pay_options_new.css',
            // spriteClass: 'sprite-new sprite-32',

            spriteSrc: (avaPage.isExpressCheckout ? 'https://secure.avangate.com/images/merchant/6531b32f8d02fece98ff36a64a7c8260/avacart-payment-options-sprite-32.css' : 'https://secure.avangate.com/images/merchant/6531b32f8d02fece98ff36a64a7c8260/avacart-payment-options-sprite-48.css'),
            spriteClass: (avaPage.isExpressCheckout ? 'sprite-32' : 'sprite-48'),

            // spriteSrc: 'https://secure.avangate.com/images/merchant/6531b32f8d02fece98ff36a64a7c8260/sprite-64x44-new-style.css',
            // spriteClass: 'sprite-new sprite-64',
            cardIconsPlaceholder: '.payment-options-card-icons',
            cardIconsSpriteSrc: 'https://secure.avangate.com/images/merchant/6531b32f8d02fece98ff36a64a7c8260/avacart-payment-options-sprite-32.css',
            cardIconsSpriteClass: 'sprite-32',

            // === CUSTOM LAYOUTS ===
           // -------- Settings for 'layout-1': --------  
            radioOptions: (avaPage.isExpressCheckout ? 3 : 0),
            showRadioButtons: avaPage.isExpressCheckout,
            showIcons: true,
            // -------- Settings for 'layout-2': -------- 
            //            radioOptions: 2,
            //            showRadioButtons: false,
            //            showIcons: true,
            // -------- Settings for 'layout-3': --------
            //            radioOptions: 3,
            //            showRadioButtons: true,
            //            showIcons: false,
            // -------- Settings for 'layout-4': --------
            //            showRadioButtons: true,
            //            showLabels: false,
            //            showIcons: true,
            //            spriteSrc: 'https://secure.avangate.com/images/merchant/6531b32f8d02fece98ff36a64a7c8260/avacart-payment-options-sprite-32.css',
            //            spriteClass: 'sprite-32',
            // -------- Settings for 'layout-5': --------
            //            radioOptions: 3,
            //            showIconsAfterText: true,
            //            showRadioButtons: false,
            //            cardIconsSpriteSrc: 'https://secure.avangate.com/images/merchant/6531b32f8d02fece98ff36a64a7c8260/avacart-payment-options-sprite-32.css',
            //            cardIconsSpriteClass: 'sprite-32',
            // -------- Settings for 'layout-6': --------
            //            radioOptions: 3,
            //            showRadioButtons: true,
            //            showIcons: true,
            //            showIconsAfterText: !avaPage.isExpressCheckout,
            //            showGroupedCardsIcons: false,
            //            insertPaymentInputsAfterSelectedPaymentOption: true,
            //            moveBillingCurrencyAfterPaymentOptionsSelect: !avaPage.isExpressCheckout,
            //            showMovedBillingCurrencyLabel: false,

            layout: 'layout-1'
        });
 * 
 * @author Adrian Staniloiu <adrian.staniloiu@avangate.com>
 * 
 * @todo Checkout with cart functionalities (grupCards false)
 */
 
AvaCart.Widgets.paymentOptions = {
    /** 
     * Default Wdiget settings. This will be merged with the widget initialize options object.
     *
     * @member {Object} 
     *
     * @property {string} placeholder The placeholder class or id e.g. ".payment-box", created before the initialize.
     * @property {string} spriteClass Sprite class e.g. "sprite-32"
     * @property {string} spriteSrc Sprite URL Source.
     * @property {boolean} groupCards If the all credict cards should be grouped as one payment option.
     * @property {boolean} showGroupedCardsIcons Works only if "groupCards" and "showIcons" are "true".
     * @property {boolean} autogetCardTypes If "groupCards" is "true" "autogetCardTypes" will be considered "true".
     * @property {string} cardsLabel The label that will be displayed for the grouped Credit Cards options.
     * @property {string} radioOptions Enables the radio for the options.
     * @property {string} showRadioButtons Displays the radio buttons on each payment options.
     * @property {string} showLabels Displays each payment options text label.
     * @property {string} showIcons Displays eeach payment options icon.
     * @property {string} showOptionTitle 
     * @property {string} selectOtherMethods The label that will be displayed for the select other methods dropdown.
     * @property {string} cardIconsPlaceholder The external placeholder ".where-the-cards-icon" to be appended.
     * @property {string} cardIconsSpriteSrc Source of the sprite used only for credit cards icons. 
     * @property {string} cardIconsSpriteClass
     * @property {string} layout AVAILABLE OPTIONS: 'default', 'layout-1',  'layout-2', 'layout-3', 'layout-4', 'layout-5'
     * @property {string} widgetClass Adds class to main wrapper of the class.
     * @property {object} optionsOrder Order or remove payment options globally or by country. The ordering will not work if "groupCards" is enabled.<pre>
     {
        remove: [5, 32], // globally
        order: [8, 1, 15], // globally
        countries: {
            222: { //US
                remove: [14, 49, 6, 2],
                order: [1, 4, 8, 15]                        
            },
            39: { //Canada
                remove: [14, 49, 6, 2],
                order: [1, 4, 8, 15]
            }
        }
     }
     </pre>
     * @property {boolean} countryHasChanged Internal variable that is used for regenerating payment options order when country has been changed.
     * @property {boolean} moveBillingCurrencyAfterPaymentOptionsSelect Intended to work with the layout used for Kaspersky; if set to true, the billing currency selector is inserted after the "Other Payment Options" selector.
     * @property {boolean} showMovedBillingCurrencyLabel If true, the label for the moved billing currency selector is shown.
     * @property {boolean} showBillingCurrencyInHeader If true, a clone of the billing currency selector is appended to the payment options box title.
     * @property {boolean} showBillingCurrencyLabelInHeader If true, the label for the billing currency selector in header is shown.
     * @property {boolean} showLanguageInHeader If true, a clone of the language selector is appended to the payment options box title.
     * @property {boolean} showLanguageLabelInHeader If true, the label for the language selector in header is shown.
     * @property {boolean} showLanguageFlagsInHeader If true, the flags for the language selector in header are shown.
     * @property {boolean} showArrowIcon Enables custom icon ("arrowIcon", "focusArrowIcon") from using a custom font that will replace the css arrow in the selector.
     * @property {string} arrowIcon E.g. 'icon-down-open'
     * @property {string} focusArrowIcon E.g. 'icon-up-open'
     */
    settings: {
        placeholder: null,
        spriteClass: 'sprite',
        spriteSrc: null,
        excludedPaymentOptionsFromCards: ['49', '57', '58'],
        groupCards: true,
        showGroupedCardsIcons: true, // Works only if "groupCards" and "showIcons" is "true".
        autogetCardTypes: true, // If "groupCards" is "true" "autogetCardTypes" will be considered "true".
        cardsLabel: 'Credit Cards',
        radioOptions: 3,
        showRadioButtons: true,
        showLabels: true,
        showIcons: true,
        showOptionTitle: true,
        selectOtherMethods: 'Select other methods',
        cardIconsPlaceholder: null,
        cardIconsSpriteSrc: null,
        cardIconsSpriteClass: null,
        layout: 'default', // AVAILABLE OPTIONS: ['default', 'layout-1',  'layout-2', 'layout-3', 'layout-4', 'layout-5']
        widgetClass: null,
        optionsOrder: {},
        countryHasChanged: false,
        insertPaymentInputsAfterSelectedPaymentOption: false,
        moveBillingCurrencyAfterPaymentOptionsSelect: false,
        showMovedBillingCurrencyLabel: true,
        showBillingCurrencyInHeader: false,
        showBillingCurrencyLabelInHeader: true,
        showLanguageInHeader: false,
        showLanguageLabelInHeader: true,
        showLanguageFlagsInHeader: false,
        showArrowIcon: false,
        arrowIcon: 'icon-down-open',
        focusArrowIcon: 'icon-up-open'
    },
    /** 
     * The HTML content that will parsed by nunjucks. This should be mapped to "AvaCart.Widgets.paymentOptions" namespace from "gulpfile.js"
     * @member {string} 
     */
    template: null,
    
    /** 
     * Helper Functions.
     * @namespace AvaCart.Widgets.paymentOptions.functions 
     * @memberof AvaCart.Widgets.paymentOptions
     */
    functions: {
        /**
         * @version 1.0.0
         * @since   1.0.0
         * @param {bolean} cc If {@link AvaCart.Widgets.paymentOptions.settings.groupCards} is true.
         * @returns {string}
         */
        getSelectedPaymentOptionValue: function (cc, settings) {
            var output = $('#payment').val();

            if (cc && typeof _t_settings.accepted_cards != 'undefined' && _t_settings.accepted_cards.hasOwnProperty(output) && ($.inArray(output, settings.excludedPaymentOptionsFromCards) === -1)) {
                output = 'cc';
            }

            return output;
        },
        
        /**
         * Change Payment Options Order
         *
         * @version 1.0.0
         *
         * @since   1.0.0
         */
        changePaymentOptionsOrder: function (settings) {
            if (typeof settings.optionsOrder !== 'undefined') {
                var billingcountry = $('#billingcountry').val();
                if (typeof settings.optionsOrder.order !== 'undefined' && settings.optionsOrder.order instanceof Array && typeof settings.optionsOrder.countries !== 'undefined' && (typeof settings.optionsOrder.countries[billingcountry] === 'undefined' || typeof settings.optionsOrder.countries[billingcountry].order === 'undefined')) {
                    var paymentOptionsOrder = settings.optionsOrder.order.slice().reverse();
                    for (i in paymentOptionsOrder) {
                        $('#payment').prepend($('#payment option[value="' + paymentOptionsOrder[i] + '"]'));
                    }
                    if (!settings.groupCards && ((omniture_vars.FIRST_CART_ACCESS == 1 && _t_settings.PAYID !== paymentOptionsOrder.slice(-1)) || settings.countryHasChanged)) {
                        this.triggerPaymentChange(paymentOptionsOrder.slice(-1));
                    } else {
                        //                        $('#payment').prepend($('#payment option:selected'));
                    }
                }
                if (typeof settings.optionsOrder.countries !== 'undefined' && settings.optionsOrder.countries.hasOwnProperty(billingcountry) && typeof settings.optionsOrder.countries[billingcountry].order !== 'undefined' && settings.optionsOrder.countries[billingcountry].order instanceof Array) {
                    var countryPaymentOptionsOrder = settings.optionsOrder.countries[billingcountry].order.slice().reverse();
                    for (i in countryPaymentOptionsOrder) {
                        $('#payment').prepend($('#payment option[value="' + countryPaymentOptionsOrder[i] + '"]'));
                    }
                    if (!settings.groupCards && ((omniture_vars.FIRST_CART_ACCESS == 1 && _t_settings.PAYID !== countryPaymentOptionsOrder.slice(-1)) || settings.countryHasChanged)) {
                        this.triggerPaymentChange(countryPaymentOptionsOrder.slice(-1));
                    } else {
                        //                        $('#payment').prepend($('#payment option:selected'));
                    }
                }
            }
        },
        /**
         * Detect Cards
         *
         * @version 1.0.0
         *
         * @since   1.0.0
         *
         * @param   {string} input
         *
         * @return  {string} Card type if found in _t_settings.accepted_cards;
         */
        getCardType: function(input) {
            var cardType;
            if (typeof input != 'string' || !input.length) {
                return false;
            }
            // VISA
            if (input.match(/^4[0-9]\d+$/)) {
                cardType = '1';
            }
            // Cart Bleue
            if (input.match(/^497[0-9]\d+$/)) {
                cardType = '17';
            }
            // MasterCard
            if (input.match(/^5[0-9]\d+$/)) {
                cardType = '1';
            }
            // Amex
            if (input.match(/^3[47]\d+$/)) {
                cardType = '4';
            }
            // Diners
            if (input.match(/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/)) {
                cardType = '5';
            }
            // Discover
            if (input.match(/^6(?:011)\d+$/)) {
                cardType = '14';
            } else if (input.match(/^6[0-9][0-9]\d+$/)) { // Visa
                cardType = '1';
            }
            // JCB
            if (input.match(/^(?:2131|1800|35\d{3})\d{11}$/)) {
                cardType = '6';
            }
            if (_t_settings.accepted_cards.hasOwnProperty(cardType)) {
                return cardType;
            } else {
                return false;
            }
        },
        /**
         * Trigger "#payment" change for all flows.
         *
         * @version 1.0.0
         *
         * @since   1.0.0
         *
         * @param   {string} value
         */
        triggerPaymentChange: function (value) {
            var value = (value === 'cc' && _t_settings && _t_settings.accepted_cards ? (function () {
                for (i in _t_settings.accepted_cards) {
                    return i;
                }
            })() : value);
            if ((omniture_vars.DESIGN_TYPE == 1) || (omniture_vars.DESIGN_TYPE == 2)) {
                $('#payment_radio_' + value).trigger('click');
            } else {
                $('#payment').val(value).trigger('change').trigger('change');
            }
        },
        /**
         * Create payment options array.
         *
         * @version 1.0.0
         *
         * @since   1.0.0
         *
         * @param   {bolean} groupCards
         *
         * @return  {array} Payment Options Array
         */
        getPaymentOptionsArray: function (settings) {
            var paymentOptions = [],
                cardsOption = {
                    id: 'cc',
                    title: settings.cardsLabel,
                    value: null,
                    checked: false,
                    cards: []
                };
            try {
                this.changePaymentOptionsOrder(settings);
            } catch (error) {
                avaLog(error)
            }
            $('#payment option').each(function (i, el) {
                var $el = $(el),
                    billingcountry = $('#billingcountry').val();
                $el.text(function (i, text) {
                    return text.replace('/Eurocard', '');
                });
                // Payment Options Order
                if (typeof settings.optionsOrder !== 'undefined') {
                    if (typeof settings.optionsOrder.remove !== 'undefined' && $.inArray(parseInt(el.value), settings.optionsOrder.remove) > -1) {
                        return;
                    }
                    if (typeof settings.optionsOrder.countries !== 'undefined' && settings.optionsOrder.countries.hasOwnProperty(billingcountry)) {
                        if (typeof settings.optionsOrder.countries[billingcountry].remove !== 'undefined' && $.inArray(parseInt(el.value), settings.optionsOrder.countries[billingcountry].remove) > -1) {
                            return;
                        }
                    }
                }
                //
                var option = {
                    id: $el.val(),
                    title: $el.text(),
                    value: $el.val(),
                    checked: $el.is(':selected')
                }
                if (settings.groupCards && _t_settings.accepted_cards.hasOwnProperty($el.val()) && ($.inArray($el.val(), settings.excludedPaymentOptionsFromCards) === -1)) {
                    if ($el.is(':selected')) {
                        cardsOption.checked = $el.is(':selected');
                        cardsOption.value = $el.val();
                    }
                    var card = {
                        title: $el.text(),
                        value: $el.val(),
                        checked: $el.is(':selected')
                    };
                    cardsOption.cards.push(card);
                } else {
                    paymentOptions.push(option);
                }
            });
            // Check if "cardsOption" has value, if not a set first value from cards.
            if (cardsOption.value == null && cardsOption.cards.length) {
                cardsOption.value = cardsOption.cards[0];
            }
            // If "groupCards" is true push and unshift paymentOptions.
            if (settings.groupCards) {
                paymentOptions.unshift(cardsOption);
            }
            // if (cardsOption.checked == false) {
            //     paymentOptions = (function(input) {
            //         var output = [],
            //             checkedOption = null;
            //         for (i in input) {
            //             if (input[i].checked) {
            //                 checkedOption = input[i];
            //             } else {
            //                 output.push(input[i]);
            //             }
            //         }
            //
            //         output.unshift(checkedOption);
            //
            //         return output;
            //     })(paymentOptions);
            // }
            return paymentOptions;
        },
        /**
         * Get card icons.
         */
        getCardIcons: function (cards, settings) {
            var output = [];
            for (i in cards) {
                output.push('<img class="payment-option-icon ' + (settings.cardIconsSpriteClass ? settings.cardIconsSpriteClass : settings.spriteClass) + ' pay_opt_' + cards[i].value + '" src="https://edge.avangate.net/images/spacer.gif" for="payment-option-radio-' + cards[i].value + '" title="' + cards[i].title + '">');
            }
            output.push('</div>');
            return output.join('');
        },
        /**
         * Update Submit Button
         *
         * @version 1.0.0
         *
         * @since   1.0.0
         */
        updateSubmitButton: function () {
            avaUpdateSubmitButton();
        },

        handleCountryChange: function (self) {
            if (self.settings.insertPaymentInputsAfterSelectedPaymentOption) {
                self.settings.countryHasChanged = true;
                $(self.settings.placeholder).after($('#order__checkout__billing__payoptions__table'));
                if (self.settings.moveBillingCurrencyAfterPaymentOptionsSelect) {
                    $('#order__checkout__form__billing__currency').insertAfter($('#payment__methods__icons'));
                }
                self.destroy();
                self.render();
                self.events();
            } else if (self.settings.moveBillingCurrencyAfterPaymentOptionsSelect) {
                $('#order__checkout__form__billing__currency').insertAfter($('#payment__methods__icons'));
            } else {
                self.settings.countryHasChanged = true;
                self.destroy();
                self.render();
                self.events();
            }
        },
        
        handleCustomLayouts: function (self, delayed) {
            var handler = function(self) {
                if (self.settings.insertPaymentInputsAfterSelectedPaymentOption) {
                    if ($('.payment-options > .payment-option.selected').length) {
                        $('.payment-options > .payment-option.selected').append($('#order__checkout__billing__payoptions__table'));
                    }
                    if ($('.payment-options > .payment-options-select.selected').length) {
                        $('.payment-options > .payment-options-select.selected').append($('#order__checkout__billing__payoptions__table'));
                    }
                    $('.order__checkout__billing__content.order__checkout__payment__content').show();
                    if ($('.order__checkout__billing__content.order__checkout__payment__content tr:visible').length === 0) {
                        $('.order__checkout__billing__content.order__checkout__payment__content').hide();
                    }
                }
            };
            if (delayed) {
                setTimeout(function() {
                    handler(self);
                }, 0);
            } else {
                handler(self);
            }
        },
        
        moveBillingCurrencyAfterPaymentOptionsSelect: function (self) {
            if (!self.settings.moveBillingCurrencyAfterPaymentOptionsSelect) return;
            if (!$('.payment-options > .payment-options-select').length) return;
            AvaCart.Widgets.billingCurrencySelector.before = function () {
                $('.payment-options > .payment-options-select').prepend('<table class="billing-currency-wrapper"></table>');
            };
            AvaCart.Widgets.billingCurrencySelector.initialize({
                placeholder: '.billing-currency-wrapper',
                showLabel: self.settings.showMovedBillingCurrencyLabel
            });
        },
        
        addBillingCurrencyInHeader: function (self) {
            if (!$('.payment__title .payment-header-currency').length) {
                var headerText = $('.payment__title').text();
                $('.payment__title').addClass('row')
                    .html('<div class="payment__title__text col-sm-6">' + headerText + '</div><div class="payment-header-currency-wrapper col-sm-6"><table class="payment-header-currency-table pull-right"><tbody class="payment-header-currency"></tbody></table></div>');
                AvaCart.Widgets.billingCurrencySelector.initialize({
                    placeholder: '.payment__title .payment-header-currency',
                    showLabel:   self.settings.showBillingCurrencyLabelInHeader
                });
                if ($('body').hasClass('rtl')) {
                    $('.payment__title .payment__title__text').addClass('col-sm-push-6');
                    $('.payment__title .payment-header-currency-wrapper').addClass('col-sm-pull-6');
                    $('.payment__title .payment-header-currency-table').removeClass('pull-right').addClass('pull-left');
                }
            }
        },

        addLanguageInHeader: function (self) {
            if (!$('.payment__title .payment-header-language').length) {
                var headerText = $('.payment__title').text();
                $('.payment__title').addClass('row').text('').prepend('<div class="payment__title__text col-sm-6">' + headerText + '</div><div class="payment-header-language-wrapper col-sm-6"></div>');
                AvaCart.Widgets.languageSelector.initialize({
                    placeholder:        '.payment-header-language-wrapper',
                    showLabel:          self.settings.showLanguageLabelInHeader,
                    showCountryFlag:    self.settings.showLanguageFlagsInHeader,
                    widgetClass:        'payment-header-language pull-right'
                });
                if ($('body').hasClass('rtl')) {
                    $('.payment__title .payment__title__text').addClass('col-sm-push-6');
                    $('.payment__title .payment-header-language-wrapper').addClass('col-sm-pull-6');
                    $('.payment__title .payment-header-language').removeClass('pull-right').addClass('pull-left');
                }
            }
        }
    },
    render: function () {
        // Hide old select.
        $('#payment__methods').hide();
        var context = {
            settings: this.settings,
            paymentOptions: this.functions.getPaymentOptionsArray(this.settings)
        }
        // context.selectedOption = (function(paymentOptions){
        //     for (i in paymentOptions) {
        //         if (paymentOptions[i].checked == true) {
        //             return paymentOptions[i];
        //         }
        //     }
        // })(context.paymentOptions);
        var html = nunjucks.renderString(this.template, context);
        $(this.settings.placeholder).append(html);
        if (this.settings.spriteSrc) {
            avaAppendStyle(this.settings.spriteSrc);
        }
        if (this.settings.cardIconsPlaceholder && this.settings.groupCards) {
            if (this.settings.cardIconsSpriteSrc) {
                avaAppendStyle(this.settings.cardIconsSpriteSrc);
            }
            var cards = (function (input) {
                for (i in input) {
                    if (input[i].id == 'cc') {
                        return input[i].cards;
                    }
                }
            })(this.functions.getPaymentOptionsArray(this.settings));
            $(this.settings.cardIconsPlaceholder).append(this.functions.getCardIcons(cards, this.settings));
        }
        
        if (this.settings.showBillingCurrencyInHeader) {
            this.functions.addBillingCurrencyInHeader(this);
        }
        if (this.settings.showLanguageInHeader) {
            this.functions.addLanguageInHeader(this);
        }
        
        this.functions.handleCustomLayouts(this, true);
        this.functions.moveBillingCurrencyAfterPaymentOptionsSelect(this);
    },
    events: function () {
        var self = this;
        // Bind click event for select method.
        if ($('.payment-options-select').length) {
            $('#payment-options-select-selected-option').click(function (event) {
                stopEvent(event);
                $('.payment-options-select-list').removeClass('active');
                $('.payment-options-select-list').slideToggle('fast', function () {
                    $('#payment-options-select-selected-option').removeClass('focus');
                    if (self.settings.showArrowIcon) {
                        $('#payment-options-select-selected-option .select-box-arrow').removeClass(self.settings.focusArrowIcon).addClass(self.settings.arrowIcon);
                    }
                    if ($(this).is(':visible')) {
                        $(this).addClass('active');
                        $('#payment-options-select-selected-option').addClass('focus');
                        if (self.settings.showArrowIcon) {
                            $('#payment-options-select-selected-option .select-box-arrow').removeClass(self.settings.arrowIcon).addClass(self.settings.focusArrowIcon);
                        }
                    }
                });
            });
            $('html').click(function (event) {
                $('.payment-options-select-list').removeClass('active').slideUp();
                $('#payment-options-select-selected-option').removeClass('focus');
                if (self.settings.showArrowIcon) {
                    $('#payment-options-select-selected-option .select-box-arrow').removeClass(self.settings.focusArrowIcon).addClass(self.settings.arrowIcon);
                }
            });
        }
        // Trigger payment options change callback.
        try {
            AvaCart.Widgets.selectBoxes.resetSelectBoxesSelectedValues(AvaCart.Widgets.selectBoxes.settings);
            self.functions.handleCustomLayouts(self, false);
            self.functions.updateSubmitButton();
            self.paymentOptionChangeCallback();
        } catch (error) {
            avaLog(error);
        }
        // Bind click event on payment option radio.
        $('.payment-option-radio').click(function (event) {
            event.stopPropagation();
            var $this = $(this);
            // Treat case for ".payment-options-select-list".
            if ($this.parents('.payment-options-select-list:first').length) {
                $('.payment-options-select-selected-option-value').html($this.parents('.payment-option:first').find('label').html());
                $('.payment-options-select-selected-option-value').parents('.payment-options-select:first').addClass('selected');
            } else {
                $('.payment-options-select-selected-option-value').html(self.settings.selectOtherMethods);
                $('.payment-options-select-selected-option-value').parents('.payment-options-select:first').removeClass('selected');
            }
            // Add "selected" class to ".payment-option".
            $('.payment-option').removeClass('selected')
            $this.parents('.payment-option:first').addClass('selected');
            // Trigger payment change.
            self.functions.triggerPaymentChange($this.data('payment-option-value'));
            // Trigger payment options change callback.
            try {
                AvaCart.Widgets.selectBoxes.resetSelectBoxesSelectedValues(AvaCart.Widgets.selectBoxes.settings);
                self.functions.handleCustomLayouts(self, false);
                self.functions.updateSubmitButton();
                self.paymentOptionChangeCallback();
            } catch (error) {
                avaLog(error);
            }
        });
        if ($('.payment-options-select-list > li.selected').length) {
            $('.payment-options-select-selected-option-value').html($('.payment-options-select-list > li.selected').find('label').html());
        }
        if (self.settings.radioOptions == 0) {
            $('.payment-options-select').addClass('selected');
        }
        if ($('.payment-options-select-list .payment-option.selected').length === 1) {
            $('.payment-options-select').addClass('selected');
        }
        // SSO in cart case.
        if ($('#payment_radio_NEW').length && _t_settings.hasOwnProperty('sso_cards')) {
            if ($('#payment_radio_NEW').not(':checked')) {
                $(self.settings.placeholder).hide();
            }
            $('#existing_cards_radios_wrap .payment_radio_input').live('click', function (event) {
                if (event.target.id == 'payment_radio_NEW') {
                    $(self.settings.placeholder).show();
                } else {
                    $(self.settings.placeholder).hide();
                }
            });
        }
        // Bind auto detect cards.
        if (self.settings.groupCards && self.settings.autogetCardTypes) {
            $('#tiCNumber').addClass('card-number-input-autodetect ' + self.settings.cardIconsSpriteClass).bind('blur keyup', function () {
                if (($('#payment').val() === '57') || ($('#payment').val() === '58')) return;
                if (self.functions.getCardType(this.value)) {
                    if (self.settings.groupCards) {
                        $('#payment-option-radio-cc').attr('checked', true);
                        self.functions.triggerPaymentChange(self.functions.getCardType(this.value));
                    } else {
                        $('#payment-option-radio-' + self.functions.getCardType(this.value)).trigger('click');
                    }
                    $(self.settings.cardIconsPlaceholder).find('img').removeClass('active');
                    $(self.settings.cardIconsPlaceholder).find('.pay_opt_' + self.functions.getCardType(this.value)).addClass('active');
                }
                // Trigger payment options change callback.
                try {
                    AvaCart.Widgets.selectBoxes.resetSelectBoxesSelectedValues(AvaCart.Widgets.selectBoxes.settings);
                    // self.functions.handleCustomLayouts(self, false);
                    self.functions.updateSubmitButton();
                    self.paymentOptionChangeCallback();
                } catch (error) {
                    avaLog(error);
                }
            });
        }
    },
    destroy: function () {
        $(this.settings.placeholder).empty();
        $(this.settings.cardIconsPlaceholder).empty();
    },
    before: function () { },
    callback: function () { },
    paymentOptionChangeCallback: function () {},
    /**
    * @method
    * @param {object} [options=AvaCart.Widgets.paymentOptions.settings] - Check this.settings for default options.
    */
    initialize: function (options) {
        try {
            var self = this;
            $.extend(true, self.settings, options); // extend default settings
            self.before();
            self.render();
            self.events();
            self.callback();
            // Reinitialize payment options after "#billingcountry" change.
            $('#billingcountry').change(function () {
                self.functions.handleCountryChange(self);
            });
        } catch (error) {
            avaLog(error);
        }
    }
}