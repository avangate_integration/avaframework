/**
 * Display CVC Inline
 * @memberof AvaCart.Widgets
 * @namespace displayCVVInline
 * @version 1.0.0
 * @since   1.0.0 
 * 
 * @author  Alexandru Salajan (alexandru.salajan@avangate.com)
 * 
 */
AvaCart.Widgets.displayCVVInline = {
    settings: {
        placeholder : null,
        label       : ''
    },
    template: null,
    functions: {},
    render: function () {
        $('#card__data__cvvc').addClass('hidden');
        this.settings.placeholder.append($('#tiCVV').attr('placeholder', this.settings.label).attr('title', this.settings.label));
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    /**
    * @method
    * @param {object} [options=AvaCart.Widgets.displayCVVInline.settings] - Check this.settings for default options.
    */
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}