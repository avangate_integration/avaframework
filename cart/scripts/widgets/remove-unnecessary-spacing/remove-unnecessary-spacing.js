/**
 * Floating Labels
 *
 * @author Alexandru Salajan (alexandru.salajan@avangate.com)
 */
AvaCart.Widgets.removeUnnecessarySpacing = {
    settings: {
        placeholder: null,
    },
    template: null,
    render: function () {
        $('#order__update__cart').hide(); //.parent('td').parent('tr').hide();
        $('#samedelivery:hidden').parent('td').parent('tr').hide();
        
        $('.last-spaced-row').removeClass('last-spaced-row');
        if (!avaPage.isExpressCheckout) {
            $('#order__checkout__billing__info__content__tbody > tr:visible').eq(-1).addClass('last-spaced-row');
        }
        $('#credit__cart__fields__container > tr:visible').eq(-1).addClass('last-spaced-row');
        $('.addFieldsOrderWrap > tr:visible').eq(-1).addClass('last-spaced-row');
        $('.order__checkout__summary > tbody > tr:visible').eq(-1).addClass('last-spaced-row');
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
