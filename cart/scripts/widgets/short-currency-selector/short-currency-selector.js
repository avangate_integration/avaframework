/**
 * Short Currency Selector
 *
 * @memberof AvaCart.Widgets
 * @namespace shortCurrencySelector
 * @version 1.0.0
 * @since   1.0.0 
 * @example <caption>Change currency selector selector to only show currency abbreviations</caption>
 *
        AvaCart.Widgets.shortCurrencySelector.initialize({
            placeholder: '#order__header__currencies'
        });
 * 
 * @author  Alexandru Salajan (alexandru.salajan@avangate.com)
 * 
 */
AvaCart.Widgets.shortCurrencySelector = {
    /** 
     * Default Wdiget settings. This will be merged with the widget initialize options object.
     *
     * @member {Object} 
     *
     * @property {string} placeholder 
     */
    settings: {
        placeholder: null
    },
    template: null,
    functions: {},
    render: function () {
        $(this.settings.placeholder).find('.selected-option').text($(this.settings.placeholder).find('option[selected]').val());
        $(this.settings.placeholder).find('option').each(function () {
            $(this).text($(this).val());
        });
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    /**
    * @method
    * @param {object} [options=AvaCart.Widgets.shortCurrencySelector.settings] - Check this.settings for default options.
    */
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}