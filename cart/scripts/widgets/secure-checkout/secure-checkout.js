/**
 * Secure Checkout
 * @memberof AvaCart.Widgets
 * @namespace secureCheckout
 * @version 1.0.0
 * @since   1.0.0 
 * @example <caption>Render the Secure Checkout</caption>
 *
        AvaCart.Widgets.secureCheckout.before = function() {
            $('.order__checkout').append('<li id="order__secure__checkout" class="col-md-12"></li>');
        };
        AvaCart.Widgets.secureCheckout.initialize({
            placeholder: '#order__secure__checkout',
            headerLabel: __order_widgets.SECURE_CHECKOUT,
            headerIconClass: 'icon-lock',
            showBoxTitle: false,
            showTitle: true,
            showBox: true,
            showSealsAfterText: true,
            avangateLogoSrc: 'https://secure.avangate.com/images/merchant/67caec8041b2d689a5035d3bf441c34c/avangate-logo.png',
            content: ''
        });
 * 
 * @author  Alexandru Salajan (alexandru.salajan@avangate.com)
 * 
 */
AvaCart.Widgets.secureCheckout = {
    settings: {
        placeholder         : null,

        headerLabel         : null,
        headerIconClass     : 'icon-lock',
        showBox             : false,
        showBoxTitle        : false,
        showTitle           : true,

        avangateLogoSrc     : null,
        content             : null,
        showSealsAfterText  : false,

        widgetClass         : null
    },
    template: null,
    functions: {},
    render: function () {
        var context = {
            settings: this.settings
        }
        
        var html = nunjucks.renderString(this.template, context);
        $(this.settings.placeholder).append(html);
        $('.secure-checkout-seals').append($('a[href*="mcafeesecure.com"]')).append($('#order__secure__seal a'));
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    /**
    * @method
    * @param {object} [options=AvaCart.Widgets.secureCheckout.settings] - Check this.settings for default options.
    */
    initialize: function (options) {
        if (typeof __order_widgets == 'undefined') return false;
        if (typeof __order_widgets.SECURE_CHECKOUT == 'undefined') return false;
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}