/**
 * Backup CD Design
 * @memberof Widgets
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaCart.Widgets.customBackupCDDesign = {
    settings: {
        placeholder : 'table.order__backupcd__option',
        
        btnIcon     : 'cd',
        labelIcon   : null,
        addLabel    : 'Add to cart',
        showButtonAfterText : false,
        btnIconContent      : null,
        
        inputIDs    : []
    },
    template: null,
    functions: {
        initialSetup: function (settings) {
            var original_input, original_input_id;
            $(settings.placeholder).each( function (index, Element) {
                $(this).find('label').attr('for', '');
                
                if (settings.labelIcon) {
                    $(this).find('label').prepend('<span class="icon icon-' + settings.labelIcon + '"></span>');
                }
                
                original_input = $(this).find('td:first input');
                original_input_id = $(original_input).attr('id');
                settings.inputIDs.push(original_input_id);
                $(original_input).addClass('backup-cd-original-input');
                
                // Remove old icon
                $(this).find('.order__backupcd__icon').parent('td').remove();
            });
        }
    },
    render: function () {
        this.functions.initialSetup(this.settings);
        var context = {
            settings: this.settings
        }
        var html;
        var template = this.template;
        var settings = this.settings;
        $(settings.placeholder).each( function (index, Element) {
            context.index = index;
            html = nunjucks.renderString(template, context);
            if (settings.showButtonAfterText) {
                $(this).find('label').append(html);
                // $(this).find('td:first').remove();
                $(this).find('td:first').attr('style', 'display: none !important;');
            } else {
                $(this).find('td:first').append(html);
            }
        });
    },
    events: function () {
        // Add click handler for Add button
        $('.backup-cd-new-input').click(function () {
            $($(this).attr('data-trigger')).attr('checked', true );
            $('#Update').click();
        });
    },
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        if ( (typeof __order_widgets == 'undefined') || (typeof __order_widgets.ADD_LABEL == 'undefined') ) return false;
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}