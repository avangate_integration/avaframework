/**
 * Cross Selling
 *
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaCart.Widgets.crossSelling = {
    settings: {
        placeholder: null,
        crossSellProducts: null,
        productShortDescription: '',
        showProductDescription: false,
        showProductDescriptionInPopup: true,    // if true, the product description will be shown in a popup
        // else, the product description will be shown below the product name
        displayCurrency: null,
        billingCurrency: null,
        showProductImage: true,
        picSize: '80',
        headerLabel: 'We also recommend',
        addToCartLabel: 'Add to cart',
        addToCartIconContent: '+',
        addToCartIconClass: 'icon-plus',
        layout: 'display-vertical', // AVAILABLE OPTIONS: [display-horizontal, display-vertical, condensed],
        autoOptimizeLayoutDisplay: true,
        maxProductsPerRow: 4,
        showDiscount: true,
        discountPercentage: true,
        showProductLink: true,
        showBox: true,
        widgetClass: null
    },
    template: null,
    functions: {
        initialSetup: function (settings) {
            var nrOfCrossSellProducts = parseInt(avaObjSize(cross_sell_products));
            if (settings.layout === 'display-vertical') {
                if (nrOfCrossSellProducts <= 2 && settings.autoOptimizeLayoutDisplay) {
                    settings.layout = 'display-horizontal';
                } else if (nrOfCrossSellProducts < parseInt(settings.maxProductsPerRow)) {
                    settings.maxProductsPerRow = nrOfCrossSellProducts;
                }
            }
            for (var i in settings.crossSellProducts) {
                if (typeof settings.crossSellProducts[i].ProductShortDescription != 'undefined') {
                    settings.crossSellProducts[i].ProductDescription = $.trim(settings.crossSellProducts[i].ProductShortDescription);
                    /*try {
                        if (!($.browser.msie && parseInt($.browser.version) < 9)) {
                            settings.crossSellProducts[i].ProductDescription = settings.crossSellProducts[i].ProductDescription.length > 120 ? (settings.crossSellProducts[i].ProductDescription.substring(0, 120) + '... <div class="x-sell-long-description-popup"><div class="x-sell-long-description">' + settings.crossSellProducts[i].ProductDescription + '</div></div>') : settings.crossSellProducts[i].ProductDescription;
                        }
                    } catch (error) {
                        avaLog(error);
                    }*/
                }
            }
        }
    },
    render: function () {
        this.functions.initialSetup(this.settings);
        var context = {
            settings: this.settings
        }

        var html = nunjucks.renderString(this.template, context);
        $(this.settings.placeholder).append(html);
    },
    events: function () {
        // Add click handler for Add button
        $('.add-cross-sell').click(function () {
            $($(this).attr('data-trigger')).next().click();
        });
        
                
        // Adjust Height On Vertical Display.
        $('#x-sell.display-vertical').each(function() {
            var cellHeight = 0;
              $(this).find('.x-sell-prod').each(function() {
                    cellHeight = $(this).height();
                    if ($(this).height() > cellHeight) {
                        cellHeight = $(this).height();
                    }
                }).height(cellHeight);
        });
    },
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        if (typeof cross_sell_products == 'undefined') return false;
        if (avaObjSize(cross_sell_products) === 0) return false;
        if ($('#order__cross__sell__content, #order__crossselling').length === 0) return false;
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}