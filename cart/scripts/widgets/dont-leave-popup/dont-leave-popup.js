/**
 * Don't Leave Popup
 * @memberof AvaCart.Widgets
 * @namespace paymentOptions
 * @version 1.0.0
 * @since   1.0.0 
 * @author Alexandru Salajan <alexandru.salajan@avangate.com>
 * @example <caption>Activate Don't Leave Popup</caption>
        AvaCart.Widgets.dontLeavePopup.initialize({
            alertMessage: 'We are sad to see you leave this page \n\n  Get a 10% discount now!',
            imagesURL: '/images/merchant/edfbe1afcf9246bb0d40eb4d8027d90f/',
            imageName: AvaCart.Functions.getTranslation('popup_img', 'pop-up_en.jpg'),
            discountCoupon: 'Do not leave',
            CART_PRODUCTS: omniture_vars.CART_PRODUCTS
        });
 */
AvaCart.Widgets.dontLeavePopup = {
    /** 
     * Default Wdiget settings. This will be merged with the widget initialize options object.
     *
     * @member {Object} 
     *
     * @property {boolean} displayTheDialog Variable used by the widget logic to decide if the popup should be displayed or not
     * @property {string} alertMessage The message shown in the alert box
     * @property {string} imagesURL URL to the vendor's media center (the folder where the backround images are kept)
     * @property {string} imageName Name of the image used for the popup's background 
     * @property {string} discountCoupon The coupon code
     * @property {boolean} useCookie If true, a cookie will be used to only show the popup once
     * @property {object} CART_PRODUCTS omniture_vars.CART_PRODUCTS
     */
    settings: {
        displayTheDialog: true,
        alertMessage: '',

        imagesURL: '',
        imageName: '',

        discountCoupon: '',

        useCookie: true,

        CART_PRODUCTS: null
    },
    template: null,
    functions: {
        applyDiscount: function (self) {
            var discount_link = 'https://secure.avangate.com/order/checkout.php?';
            var prods = 'PRODS=';
            var qty = '&QTY=';
            if (typeof CART_PRODUCTS !== 'undefined') {
                for (var prod_index in CART_PRODUCTS) {
                    if (CART_PRODUCTS[prod_index].hasOwnProperty(ProductID)) {
                        prods += CART_PRODUCTS[prod_index].ProductID;
                        qty += $('#order__listing__row__' + CART_PRODUCTS[prod_index].ProductID + ' .order__listing__item__qty input').val();
                        if (prod_index < (CART_PRODUCTS.length - 1)) {
                            prods += ',';
                            qty += ',';
                        }
                    }
                }
            }
            discount_link += prods;
            discount_link += qty;
            discount_link += '&COUPON=' + self.settings.discountCoupon;
            window.location = discount_link;
        },

        prepareDontLeavePopup: function (self) {
            //if ( ( (avaPage.current==='cart') || (avaPage.current==='checkout') || (avaPage.current==='verify') ) && ($('.order__product__discount').length===0) ){
            $('.img-popup-discount').css('background-image', 'url("' + self.settings.imagesURL + self.settings.imageName + '")');
            //Build the popup.
            $('#dont-leave-popup').dialog({
                bgiframe: true,
                modal: true,
                position: ['center', 150],
                autoOpen: false,
                resizable: false,
                dialogClass: 'dont-leave-popup'
            });
            $('#Checkout').mouseenter(function () { self.settings.displayTheDialog = 0; });
            $('#Checkout').mouseleave(function () { self.settings.displayTheDialog = 1; });
            $('#language').bind('click', function () { self.settings.displayTheDialog = 0; });
            $('#display_currency').bind('click', function () { self.settings.displayTheDialog = 0; });
            $('#Update').mouseenter(function () { self.settings.displayTheDialog = 0; });
            $('#Update').mouseleave(function () { self.settings.displayTheDialog = 1; });
            $('.order__checkout__button__container .order__finish__button').mouseenter(function () { self.settings.displayTheDialog = 0; });
            $('.order__checkout__button__container .order__finish__button').mouseleave(function () { self.settings.displayTheDialog = 1; });
            $('a').bind('click', function () { self.settings.displayTheDialog = 0; });
            $('#wia-text').mouseenter(function () { self.settings.displayTheDialog = 0; });
            $('#wia-text').mouseleave(function () { self.settings.displayTheDialog = 1; });
            $('input').mouseenter(function () { self.settings.displayTheDialog = 0; });
            $('input').mouseleave(function () { self.settings.displayTheDialog = 1; });
            $('#AuthorizeButton').mouseenter(function () { self.settings.displayTheDialog = 0; });
            $('#AuthorizeButton').mouseleave(function () { self.settings.displayTheDialog = 1; });
            $('#dont-leave-popup-discount-link').mouseenter(function () { self.settings.displayTheDialog = 0; });
            $('#dont-leave-popup-discount-link').mouseleave(function () { self.settings.displayTheDialog = 1; });
            $('.order__listing__item__qty').mouseenter(function () { self.settings.displayTheDialog = 0; });
            $('.order__listing__item__qty').mouseleave(function () { self.settings.displayTheDialog = 1; });
            $('.add_custom_option').mouseenter(function () { self.settings.displayTheDialog = 0; });
            $('.add_custom_option').mouseleave(function () { self.settings.displayTheDialog = 1; });
            $('.order__button').mouseenter(function () { self.settings.displayTheDialog = 0; });
            $('.order__button').mouseleave(function () { self.settings.displayTheDialog = 1; });
            $('select').mouseenter(function () { self.settings.displayTheDialog = 0; });
            $('select').mouseleave(function () { self.settings.displayTheDialog = 1; });
            $('select[name=language]').mouseenter(function () { self.settings.displayTheDialog = 0; });
            //  $('select[name=language]').mouseleave(function(){ self.settings.displayTheDialog=1; });
            $('select[name=display_currency]').mouseenter(function () { self.settings.displayTheDialog = 0; });
            //$('select[name=display_currency]').mouseleave(function(){ self.settings.displayTheDialog=1; });
            if (!navigator.userAgent.toLowerCase().match('msie')) {
                $('.table-main').mouseenter(function () { self.settings.displayTheDialog = 0; });
                $('.table-main').mouseleave(function () { self.settings.displayTheDialog = 1; });
            }
            $('select[name=language]').click(function () { self.settings.displayTheDialog = 0; });
            $('select[name=display_currency]').click(function () { self.settings.displayTheDialog = 0; });
            $('#frmCheckout').submit(function () { self.settings.displayTheDialog = 0; });

            $('select[name="language"], select[name="display_currency"], .table-custom-qty input, #billingcountry, .order__button.add_custom_option, .submit-button').click(function () { self.settings.displayTheDialog = false; });
      
            /*displayVatDialog=true;
            $('#fiscalcode').blur(function(){
            displayVatDialog = false; 
            setTimeout(function(){
            displayVatDialog = true}, 3000);
            
            })
            
            $('body').hover(function(){
            setTimeout(function(){
            displayVatDialog = true}, 3000);
            })
            */
        }
    },
    render: function () {
        var context = {
            settings: this.settings
        };

        var html = nunjucks.renderString(this.template, context);
        $('body').append(html);

        this.functions.prepareDontLeavePopup(this);
    },
    events: function () {
        var self = this;
        window.onbeforeunload = function (event) {
            if (self.settings.useCookie) {
                if (($.cookie('modal_popup_done') !== 'done') && self.settings.displayTheDialog) {
                    $('#dont-leave-popup').dialog('open');
                    $.cookie('modal_popup_done', 'done');
                    return self.settings.alertMessage;
                }
            } else {
                $('#dont-leave-popup').dialog('open');
                return self.settings.alertMessage;
            }
        };

        $('#dont-leave-popup-close-button').click(function () {
            $('.dont-leave-popup .ui-dialog-titlebar-close').click();
        });
        $('body').click(function () {
            $('.dont-leave-popup .ui-dialog-titlebar-close').click();
        });
        $('.dont-leave-popup .description').click(function (event) {
            event.stopPropagation();
        });

        $('#dont-leave-popup-discount-link').click(function (event) {
            event.preventDefault();
            self.functions.applyDiscount(self);
        });
    },
    destroy: function () { },
    before: function () { },
    callback: function () { },
    initialize: function (options) {
        try {
            if (!((avaPage.isCart || avaPage.isCheckoutPage) && ($('.order__product__discount').length === 0))) {
                return;
            }
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
};