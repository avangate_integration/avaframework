/**
 * Remove Products
 * 
 * @author Adrian Staniloiu <adrian.staniloiu@avangate.com>
 * 
 * @type {Object}
 */
AvaCart.Widgets.removeProducts = {
    settings: {
        view: 'img',
        removeButtonContent: 'x'
    },
    template: {},
    render: function () {
        var self = this;
        $('.order__listing__item__remove input[type="checkbox"]').parent('td.order__listing__item__remove').each(function () {
            var $this = $(this);
            switch(self.settings.view) {
                case 'img':
                    $this.append('<div class="remove-product"><img src="'+ SPACER_IMAGE +'" class="remove-product-button ' + self.settings.view + '" data-remove="' + $this.find('[type="checkbox"]').hide().attr('name') +'"/></div>');
                    break;
                case 'text':
                    $this.append('<div class="remove-product"><div class="remove-product-button ' + self.settings.view + '" data-remove="' + $this.find('[type="checkbox"]').hide().attr('name') +'">' + self.settings.removeButtonContent + '</div></div>');
                    break;
            }
            
        });
    },
    events: function () {
        $('.remove-product-button').click(function () {
            $('[name="' + $(this).data('remove') + '"]').attr('checked', true);
            $('#Update').trigger('click');
        });
    },
    destroy: function () {
        $('.order__listing__item__remove input[type="checkbox"]').show();
        $('.remove-product').remove();
    },
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}