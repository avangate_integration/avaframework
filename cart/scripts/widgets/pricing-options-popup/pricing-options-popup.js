/**
 * Pricing Options Popup
 * 
 * @memberof AvaCart.Widgets
 * @namespace pricingOptionsPopup
 * @version 1.0.0
 * @author Adrian Staniloiu <adrian.staniloiu@avangate.com>
 * @type    {Object}
 *  @example <caption>Initialize the widget:</caption>
 *     AvaCart.Widgets.pricingOptionsPopup.before = function() {
            for(var i in omniture_vars.CART_PRODUCTS) {
                if (omniture_vars.CART_PRODUCTS.hasOwnProperty(i)) {
                    $('#order__listing__row__' + omniture_vars.CART_PRODUCTS[i].ProductID).find('.order__product__options__text a').attr('data-product-id', omniture_vars.CART_PRODUCTS[i].ProductID);
                }
            }
        };
 *   AvaCart.Widgets.pricingOptionsPopup.initialize({
 *      hook: '.order__product__options__text a',
 *      CURRENCY: omniture_vars.BILLING_CURRENCY,
 *      UPDATE_CART: $('#Update').val()
 *   });
 */
AvaCart.Widgets.pricingOptionsPopup = {
    /** 
     * Default Wdiget settings. This will be merged with the widget initialize options object.
     *
     * @member {Object} 
     *
     * @property {string} hook The hook selection, created before the initialize or an existing one, it must have the attribute data-product-id="PRODUCTID" set.
     * @property {string} coupon Latest coupon used that was applied successfully.
     * 
     */
    settings: {
        hook: '.order__product__options__text a',
        priceFormat: '{{currency}} {{price}}',
        CURRENCY: null,
        loader: '<img src="https://secure.avangate.com/images/merchant/67caec8041b2d689a5035d3bf441c34c/ava-cart-small-loader.gif" />',
        currencySigns: {
            USD: '$',
            EUR: '€',
            GBP: '£'
        },
        useSigns: true,
        UPDATE_CART: 'Update Cart',
        dialogArgs: {
            width: 'auto',
            modal: false,
            autoOpen: false,
            hide: 'explode',
            draggable: false,
            resizable: false,
            dialogClass: 'pricing-options-popup-dialog',
            dialogTitle: ''
        },
        coupon: null
    },
    render: function () {
        var self = this,
            context = self.settings;
        context.products = (typeof omniture_vars != 'undefined' && avaObjSize(omniture_vars.CART_PRODUCTS) > 0 ? omniture_vars.CART_PRODUCTS : {});

        if (typeof product_options != 'undefined' && avaObjSize(product_options) > 0) {
            try {
                for (var m in product_options) {
                    for (var y in product_options[m]) {
                        var productPricingOptionsTree = self.functions.getProduct(m).PricingOptionsTree;
                        for (var x in product_options[m][y].Options) {
                            if (product_options[m][y].GroupType == 'INTERVAL') {
                                try {
                                    product_options[m][y].Options[x].min = product_options[m][y].Options[x].OptionText.split(' - ')[0];
                                    product_options[m][y].Options[x].max = product_options[m][y].Options[x].OptionText.split(' - ')[1];
                                } catch (error) {
                                    avaLog(error);
                                }
                                try {
                                    product_options[m][y].Options[x].val = productPricingOptionsTree[y][x].Value.split('=')[1];
                                    product_options[m][y].Options[x].queryVar = productPricingOptionsTree[y][x].Value.split('=')[0];
                                } catch (error) {
                                    avaLog(error);
                                }
                            }
                        }
                    }
                }
            } catch (error) {
                avaLog(error);
            }
            for (var i in context.products) {
                if (product_options.hasOwnProperty(context.products[i].ProductID)) {
                    context.products[i].PricingOptions = product_options[context.products[i].ProductID];
                }
            }
        }
        if ($(this.settings.hook).length) {
            $('body').append(nunjucks.renderString(self.template, context));
        }
        
        // Create dialogs for each hook.
        $(this.settings.hook).each(function () {
            var $el = $(this),
                prod = self.functions.getProduct($el.data('product-id')),
                dialogArgs = $.extend(self.settings.dialogArgs, {
                    title: prod.ProductName,
                });
            if (self.settings.dialogArgs.modal) {
                dialogArgs.dialogClass += ' pricing-options-popup-modal';
            }
            $('.pricing-options-popup-' + prod.ProductID).dialog(dialogArgs);
            self.functions.getOptionsPrice({
                PRODS: prod.ProductID,
                CURRENCY: self.settings.CURRENCY,
                OPTIONS: $('.pricing-options-popup-' + prod.ProductID).find('.pricing-options-query-string').val()
            },
                self.settings,
                $('.pricing-options-popup-' + prod.ProductID).find('.pricing-options-popup-price'));

            $('.pricing-options-popup-' + prod.ProductID).find('.pricing-option-field').change(function () {
                self.functions.getOptionsPrice({
                    PRODS: prod.ProductID,
                    CURRENCY: self.settings.CURRENCY,
                    OPTIONS: self.functions.getPricingOptionsQueryString(prod.ProductID)
                },
                    self.settings,
                    $('.pricing-options-popup-' + prod.ProductID).find('.pricing-options-popup-price'));
            }).end().find('.pricing-options-popup-cta').click(function() {
                window.location = ('https://' + window.location.host + '/order/checkout.php?' + 'OPTIONS' + prod.ProductID + '=' + self.functions.getPricingOptionsQueryString(prod.ProductID));
            });
        });
        
        $('.pricing-option .interval-field').bind('change blur', function() {
            var $el = $(this);
            $el.val(function(i, str) {
                str = str.replace(/[a-zA-Z]/g, '');
                if (str == '') {
                    str = $el.attr('min');
                }
                if (parseInt(str) < parseInt($el.attr('min'))) {
                    str = $el.attr('min');
                }
                if (parseInt(str) > parseInt($el.attr('max'))) {
                    str = $el.attr('max');
                }
                return str;
            });
        });
    },
    
    /** 
     * Helper Functions.
     * @namespace AvaCart.Widgets.pricingOptionsPopup.functions 
     * @memberof AvaCart.Widgets.pricingOptionsPopup
     */
    functions: {
        /**
         * Get Pricing Options Group Query String 
         */
        getPricingOptionsQueryString: function (prodID) {
            var queryString = [];
            $('.pricing-options-popup-' + prodID).find('.pricing-options-group').each(function () {
                var $el = $(this);
                if ($el.data('pricing-options-group-type') == 'RADIO') {
                    queryString.push($el.find('.pricing-option-field:checked').val());
                } else if ($el.data('pricing-options-group-type') == 'CHECKBOX') {
                    $el.find('.pricing-option-field:checked').each(function () {
                        queryString.push($(this).val());
                    });
                } else if ($el.data('pricing-options-group-type') == 'COMBO') {
                    queryString.push($el.find('.pricing-option-field option:selected').val());
                } else if ($el.data('pricing-options-group-type') == 'INTERVAL') {
                    queryString.push($el.find('.pricing-option-field').data('pricing-option-query-var') + '=' + $el.find('.pricing-option-field').val());
                }
            });
            return queryString.join(',');
        },
        
        /**
         * Get Product Object
         * 
         * @version 1.0.0
         * @since   1.0.0
         * @param {bolean} $el
         * @returns {string}
         */
        getProduct: function (prodID) {
            var prodObj = false;
            $.each(omniture_vars.CART_PRODUCTS, function (i, p) {
                if (prodID == parseInt(p.ProductID)) {
                    prodObj = p;
                    return false;
                }
            });
            return prodObj;
        },
        /**
         * Get Options Price.
         * 
         * @version 1.0.0
         * @since   1.0.0
         * @param {bolean} $el
         * @returns {string}
         */
        getOptionsPrice: function (args, settings, $placeholder) {
            var getPriceParameters = {
                QTY: 1,
                CURRENCY: null,
                PRODS: null,
                OPTIONS: null,
                COUPON: null,
                RESPONSE_TYPE: 'JSON'
            };
            getPriceParameters = $.extend(true, getPriceParameters, args);
            getPriceParameters.OPTIONS = getPriceParameters.OPTIONS.replace(/,,/g, ',');
            getPriceParameters['OPTIONS' + getPriceParameters.PRODS] = getPriceParameters.OPTIONS;
            if (settings.coupon) {
                getPriceParameters.COUPON = settings.coupon;
            }
            delete getPriceParameters['OPTIONS'];
            $.ajax({
                type: 'GET',
                url: 'https://' + window.location.host + '/action/get_price.php',
                data: getPriceParameters,
                beforeSend: function() {
                    $placeholder.html(settings.loader);
                },
                success: function (data) {
                    data = $.parseJSON(data);
                    if (!data) {
                        return '';
                    } else {
                        try {
                            var currency = getPriceParameters.CURRENCY,
                                prices = data[currency];
                                avaLog(prices);
                            if (settings.currencySigns.hasOwnProperty(currency) && settings.useSigns) {
                                currency = settings.currencySigns[currency];
                            }
                            $placeholder.html(settings.priceFormat.replace('{{currency}}', currency).replace('{{price}}', prices.ProductTotalPriceWithTaxAndDiscount));
                        } catch(error) {
                            avaLog(error);
                        }
                    }
                }
            });
        }
    },
    events: function () {
        var self = this;
        
        // Open dialogs on click.
        $(self.settings.hook).click(function (e) {
            e.preventDefault();
            var $el = $(this);
            if (!self.settings.dialogArgs.modal) {
                $('.pricing-options-popup-' + $el.data('product-id')).dialog('option', 'position', [e.clientX + 10, e.clientY + 10]);
            }
            $(window).resize(function () {
                if (!self.settings.dialogArgs.modal) {
                    $('.pricing-options-popup-' + $el.data('product-id')).dialog('option', 'position', [$el.offset().left + 20, $el.offset().top + 20]);
                } else {
                    $('.pricing-options-popup-' + $el.data('product-id')).dialog('option', 'position', 'center');
                }
            });
            $('.pricing-options-popup-' + $el.data('product-id')).dialog('open');
        });
    },
    destroy: function () { },
    before: function () { },
    callback: function () { },
    initialize: function (options) {
        try {
            $.extend(true, this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
