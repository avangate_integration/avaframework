/**
 * Heading Bullets
 *
 * @author  Alexandru Salajan (alexandru.salajan@avangate.com)
 * @type  {Object}
 */
AvaCart.Widgets.headingBullets = {
/* ==========================================================================
   SETTINGS
   ========================================================================== */
    settings: {
        placeholder: null,
        showNumbers: true
    },
    template: null,

/* ==========================================================================
   FUNCTIONS
   ========================================================================== */
    functions: {},
/* ==========================================================================
   RENDER TEMPLATE
   ========================================================================== */
    render: function () {
    /* ===================
       Template context
       =================== */
        var context = {
            settings: this.settings
        }
        
        $(this.settings.placeholder).each(function (index, elem) {
            if (context.settings.showNumbers) {
                $(this).prepend('<span class="heading-bullet">' + (index + 1) + '</span>');
            } else {
                $(this).prepend('<span class="heading-bullet"></span>');
            }
        });
    },

/* ==========================================================================
   EVENTS
   ========================================================================== */
    events: function () {},
/* ==========================================================================
   DESTROY
   ========================================================================== */
    destroy: function () {},
/* ==========================================================================
   BEFORE
   ========================================================================== */
    before: function () {},
/* ==========================================================================
   CALLBACK
   ========================================================================== */
    callback: function () {},

/* ==========================================================================
   INITIALIZATION
   ========================================================================== */
    initialize: function (options) {
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}