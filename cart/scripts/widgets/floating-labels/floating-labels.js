/**
 * Floating Labels
 *
 * @author Alexandru Salajan (alexandru.salajan@avangate.com)
 */
AvaCart.Widgets.floatingLabels = {
    settings: {
        placeholder: null,
    },
    template: null,
    render: function () {
        $(this.settings.placeholder).each(function(index, elem) {
            $(this).find('#order_person_company').parent('tbody').parent('table').before('<table class="order-person-company-wrapper"><tbody></tbody></table>');
            $('.order-person-company-wrapper tbody').html($('#order_person_company'));
            
            $(this).find('.order__checkout__form__label, .order__checkout__form__label__error').addClass('floating-label').parent().addClass('floating-label-wrapper');
            $(this).find('.order__checkout__form__input, .order__checkout__form__input__error').prev().addClass('floating-label').parent().addClass('floating-label-wrapper');
            
            $(this).find('select').parents('.order__checkout__form__input, .order__checkout__form__input__error').siblings('.floating-label').addClass('always-visible');
        });
    },
    events: function () {
        $('.floating-label-wrapper input:not([type=hidden]), .floating-label-wrapper select').bind('checkval', function() {
            var label = $(this).parents('.floating-label-wrapper').find('.floating-label');
            if ($(this).val() !== '') {
                label.addClass('visible');
            } else {
                label.removeClass('visible');
            }
        }).keyup(function() {
            $(this).trigger('checkval');
        }).focus(function() {
            $(this).parents('.floating-label-wrapper').find('.floating-label').addClass('active');
        }).blur(function() {
            $(this).parents('.floating-label-wrapper').find('.floating-label').removeClass('active');
        }).trigger('checkval');
    },
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
