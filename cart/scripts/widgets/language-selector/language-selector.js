/**
 * Language Selector
 * @description Clones the laguage selector into the specified placeholder.
 * @author Adrian Staniloiu <adrian.staniloiu@avangate.com>
 */
AvaCart.Widgets.languageSelector = {
    settings: {
        placeholder:        null,
        showLabel:          true,
        showCountryFlag:    false,
                
        widgetClass:        null
    },
    template: null,
    render: function () {
        $('#order__header__languages').clone(true)
            .removeAttr('id')
            .removeAttr('class')
            .addClass(this.settings.widgetClass)
            .appendTo($(this.settings.placeholder));
        $(this.settings.placeholder).find('*').removeAttr('id');
        $(this.settings.placeholder).find('select.language__select').removeAttr('onchange').change(function (event) {
            $('#order__header__languages select').val($(this).val());
            $('#action').val('language');
            $('#order__container form').submit();
        });
        
        // LABEL
        if (!this.settings.showLabel) {
            $(this.settings.placeholder).find('.language__label').hide();
        }
        
        // COUNTRY FLAG
        if (this.settings.showCountryFlag) {
            var countryCode = $(this.settings.placeholder).find('select.language__select').val();
            $(this.settings.placeholder).find('.header-language-select .select-box').prepend('<span class="flag flag-' + countryCode + '"></span>');
        }
    },
    events: function () {
        if (this.settings.showCountryFlag) {
            $(this.settings.placeholder).find('select').change(function(event){
                var countryCode = $(this).val();
                $(this).parents('.select-box').find('.flag').removeAttr('class').addClass('flag flag-' + countryCode);
            });
        }
    },
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}
