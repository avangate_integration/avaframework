/**
 * Display Currency Selector
 * @description Clones the display currency selector into the specified placeholder.
 * @author Alexandru Salajan (alexandru.salajan@avangate.com)
 */
AvaCart.Widgets.displayCurrencySelector = {
    settings: {
        placeholder:        null,
        showLabel:          true,
                
        widgetClass:        null
    },
    template: null,
    render: function () {
        $('#order__header__currencies').clone(true)
            .removeAttr('id')
            .removeAttr('class')
            .addClass(this.settings.widgetClass)
            .appendTo($(this.settings.placeholder));
        $(this.settings.placeholder).find('*').removeAttr('id');
        $(this.settings.placeholder).find('select.currency__select').removeAttr('onchange').change(function (event) {
            $('#order__header__currencies select').val($(this).val());
            $('#action').val('currency');
            $('#order__container form').submit();
        });
        
        // LABEL
        if (!this.settings.showLabel) {
            $(this.settings.placeholder).find('.currency__label').hide();
        }
    },
    events: function () {},
    destroy: function () {},
    before: function () {},
    callback: function () {},
    initialize: function (options) {
        try {
            // Extend default settings.
            $.extend(this.settings, options);
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}