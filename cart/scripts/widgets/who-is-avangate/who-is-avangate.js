/**
 * Who is Avangate
 * 
 * @memberof AvaCart.Widgets
 * @namespace whoisAvangate
 * 
 * @version 1.0.0
 * @since   1.0.0 
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 * @example <caption>Example for rendering.</caption>
 *         AvaCart.Widgets.whoisAvangate.initialize({
            placeholder: '#order__secure__checkout .secure-checkout-content-text',
            orderProcessedBy: __order_processed_by,
            vendorLogoSrc: omniture_vars ? omniture_vars.VENDOR_LOGO : ''
        });
 */
AvaCart.Widgets.whoisAvangate = {
    /** 
     * Default Wdiget settings. This will be merged with the widget initialize options object.
     *
     * @member {Object} 
     */
    settings: {
        placeholder: null,
        orderProcessedBy: null,
        avangateLogoSrc: 'https://secure.avangate.com/images/merchant/1ae6464c6b5d51b363d7d96f97132c75/avangate_new_logo.png',
        vendorLogoSrc: '',
        widgetClass: null
    },
    /** 
     * The HTML content that will parsed by nunjucks. This should be mapped to "AvaCart.Widgets.whoisAvangate" namespace from "gulpfile.js"
     * @member {string} 
     */
    template: null,
    
    /** 
     * Helper Functions.
     * @namespace AvaCart.Widgets.whoisAvangate.functions 
     * @memberof AvaCart.Widgets.whoisAvangate
     */
    functions: {
        /**
         * @version 1.0.0
         * @since   1.0.0
         */
        buildPopup: function () {
            // Build the popup
            $('#wia-popup').dialog({
                bgiframe: true,
                width: 580,
                modal: true,
                position: ['center', 150],
                autoOpen: false,
                dialogClass: 'wia-popup'
            });
        }
    },
    render: function () {
        var context = {
            settings: this.settings
        }
        var html = nunjucks.renderString(this.template, context);
        $(this.settings.placeholder).append(html);
        this.functions.buildPopup();
    },
    events: function () {
        $('#wia-text .link').click(function (event) {
            $('#wia-popup').dialog('open');
            event.preventDefault();
        })
    },
    destroy: function () { },
    before: function () { },
    callback: function () { },
    initialize: function (options) {
        if (typeof __order_processed_by == 'undefined') return false;
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}