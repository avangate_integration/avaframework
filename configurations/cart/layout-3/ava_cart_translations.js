/**
 * @author: Created by cezar.budulacu on 4/29/2016.
 * The global object which stores all translations for the application
 * @type {Object} templateDictionary
 */
templateDictionary = {
    en: {}
};