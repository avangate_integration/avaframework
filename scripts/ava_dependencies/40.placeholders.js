// HTML5 placeholder plugin version 1.01
// Copyright (c) 2010-The End of Time, Mike Taylor, http://miketaylr.com
// MIT Licensed: http://www.opensource.org/licenses/mit-license.php
//
// Enables cross-browser HTML5 placeholder for inputs, by first testingplaceholderSpan
// for a native implementation before building one.
//
//
// USAGE:
//$('input[placeholder]').placeholder();
// <input type="text" placeholder="username">
(function (e) {
    var t = "placeholder" in document.createElement("input");
    var n = e.browser.opera && e.browser.version < 10.5;
    e.fn.placeholder = function (r) {
        var r = e.extend({}, e.fn.placeholder.defaults, r),
            i = r.placeholderCSS.left;
        if (e("html").attr("dir") == "rtl") r.placeholderCSS.right = "5px";
        return t ? this : this.each(function () {
            var t = e(this),
                s = e.trim(t.val()),
                o = t.css("width"),
                u = t.css("height"),
                a = this.id ? this.id : "placeholder" + +(new Date),
                f = t.attr("placeholder"),
                l = e("<label for=" + a + ">" + f + "</label>");
            r.placeholderCSS.width = o;
            r.placeholderCSS.height = u;
            r.placeholderCSS.left = n && (this.type == "email" || this.type == "url") ? "11%" : i;
            l.css(r.placeholderCSS);
            t.wrap(r.inputWrapper);
            t.attr("id", a).after(l);
            if (s) {
                t.parent().find("label").hide()
            }
            t.focus(function () {
                if (!e.trim(t.val())) {
                    t.parent().find("label").hide()
                }
            });
            t.blur(function () {
                if (!e.trim(t.val())) {
                    t.parent().find("label").show()
                }
            })
        })
    };
    e.fn.placeholder.defaults = {
        inputWrapper: '<div class="placeholderSpan"  style="position:relative;"></div>',
        placeholderCSS: {
            "font-size": "13px",
            color: "#6d6d6d",
            position: "absolute",
            left: "5px",
            top: "7px",
            "overflow-x": "hidden"
        }
    }
})(jQuery);
