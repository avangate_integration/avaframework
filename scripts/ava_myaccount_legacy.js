/**
 * Log
 *
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @version 1.0.0
 * @since 1.0.0
 * @param {string} msg
 * @param {string} method
 *
 */
function avaLog() {
    'use strict';
    try {
        var args = [].slice.call(arguments, 0);
        return (typeof avaDebug != 'undefined' && avaDebug || ($ && $.cookie ? $.cookie('avaDebug') : false) && arguments.length ? (typeof console != 'undefined' ? console.log.apply(console, args) : alert(args.toSource())) : false);
    } catch (error) {
        try {
            console.log(error);
        } catch (error) {}
    }
}