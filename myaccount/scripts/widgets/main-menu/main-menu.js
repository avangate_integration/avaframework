/**
 * Backup CD Design
 *
 * @author  alexandru.salajan@avangate.com
 * @type  {Object}
 */
AvaMyAccount.Widgets.mainMenu = {
/* ==========================================================================
   SETTINGS
   ========================================================================== */
    settings: {
        placeholder     : null,
        
        links           : null,
        isLoggedIn      : null,
        
        widgetClass     : null
    },
    template: null,

/* ==========================================================================
   FUNCTIONS
   ========================================================================== */
    functions: {},
/* ==========================================================================
   RENDER TEMPLATE
   ========================================================================== */
    render: function () {
    /* ===================
       Template context
       =================== */
        var context = {
            settings: this.settings,
            
            isLinkSelected: function(linkURL) {
                linkURL = (linkURL.replace(window.location.origin, '').split('?'))[0];
                var currentURL = window.location.pathname;
                var pattern = new RegExp('^' + linkURL + '$');
                return pattern.test(currentURL);
            }
        }
        
        var html = nunjucks.renderString(this.template, context);
        $(this.settings.placeholder).append(html);
    },

/* ==========================================================================
   EVENTS
   ========================================================================== */
    events: function () {},
/* ==========================================================================
   DESTROY
   ========================================================================== */
    destroy: function () {},
/* ==========================================================================
   BEFORE
   ========================================================================== */
    before: function () {},
/* ==========================================================================
   CALLBACK
   ========================================================================== */
    callback: function () {},

/* ==========================================================================
   INITIALIZATION
   ========================================================================== */
    initialize: function (options) {
        if ( (typeof AvaMyAccount == 'undefined') || (typeof AvaMyAccount.MenuLinks == 'undefined') ) return false;
        try {
            $.extend(this.settings, options); // extend default settings
            this.before();
            this.render();
            this.events();
            this.callback();
        } catch (error) {
            avaLog(error);
        }
    }
}