// Dependencies
var gulp = require('gulp'),
	shell = require("gulp-shell");

/**
 * Documentation Gulp Tasks
 * 
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * 
 * @since 2.0.0
 * @version 2.0.0
 */
module.exports = function (config) {
   	// Documentation Prepare
	gulp.task('docs:prepare', require('./tasks/copy')(
		 config.paths.cart.documentation,	
		'./.tmp/docs/'
	));
	
    // Documentation Watch Task
	gulp.task('docs:watch', require('./tasks/watch')(
		config.listenPort, 
		[{ paths: [config.paths.cart.scripts.avaCart.framework,
					config.paths.cart.scripts.avaCart.pageStructure,
					config.paths.cart.scripts.avaCart.layouts,
					config.paths.cart.scripts.avaCart.widgets,
					config.paths.cart.scripts.avaCartLegacy,
                    config.paths.cart.documentation],
                    tasks: ['docs:prepare', 'docs'] }]
	));
    
    // Documentation Task
	gulp.task('docs', ['docs:prepare'], shell.task(
		['jsdoc -c bower_components/avaframework/gulp/config/documentation.conf']
	));
};