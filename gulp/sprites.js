// Dependencies
var gulp = require('gulp'),
	args = require('yargs').argv,
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    minifyCss = require('gulp-minify-css'),
	runSequence = require('run-sequence');
    
/**
 * Sprites Gulp Tasks
 * 
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @namespace Gulp
 * 
 * @since 2.0.0
 * @version 2.0.0
 */
module.exports = function (config) {
    /**
     * Generate Sprites - gulp task that generates and optimizes sprites sources "./dest/sprites/" folder.
     * @memberof Gulp
     * @method sprites
     * @example <caption>gulp cart:clean</caption>
     */
	gulp.task('sprites', function(cb) {
        return runSequence('sprites:images', 'sprites:minifycss');
    });
    
   /**
     * Optimize Sprites Image Sources - gulp task that optimizes sprites sources "./dest/sprites/" folder.
     * @memberof Gulp
     * @method sprites:images
     * @example <caption>gulp sprites:images</caption>
     */
	gulp.task('sprites:images', function(cb) {
        return gulp.src(config.paths.sprites + '/*.{jpg,png,svg}').pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
		.pipe(gulp.dest(config.paths.dest.sprites));
    });
    
    /**
     * Minify Sprites CSS Sources - gulp task that minify sprites CSS sources "./dest/sprites/" folder.
     * @memberof Gulp
     * @method sprites:minifycss
     * @example <caption>gulp sprites:minifycss</caption>
     */
	gulp.task('sprites:minifycss', function(cb) {
        return gulp.src(config.paths.sprites + '*.css')
            .pipe(minifyCss({
                keepSpecialComments: 1
            }))
            .pipe(gulp.dest(config.paths.dest.sprites));
    });
};