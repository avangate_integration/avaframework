// Dependencies
var gulp = require('gulp'),
	runSequence = require('run-sequence');

/**
 * Legacy Gulp Tasks
 * 
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * 
 * @since 2.0.0
 * @version 2.0.0
 */

module.exports = function (config) {
	// Cart Legacy Clean Task
	gulp.task('legacy:cart:clean', require('./tasks/clean')(
		[config.paths.dest.cart + '/legacy/']
	));
	
    /**
     * Cart Legacy - gulp task that will pre-process all the sources from "config.paths.cart.scripts.avaCartLegacy", and will deploy the js files in "dest/cart/legacy".
     * @memberof Gulp
     * @method legacy:cart
     * @example <caption>gulp legacy:cart</caption>
     */
	gulp.task('legacy:cart', ['legacy:cart:clean'], require('./tasks/scripts')(
		config.paths.cart.scripts.avaCartLegacy,
		config.paths.dest.cart + '/legacy/'
	));
	// Legacy Task
	gulp.task('legacy', function(){
		return runSequence('legacy:cart');
	});
};