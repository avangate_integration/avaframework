// Dependencies
var gulp = require('gulp'),
	fileRename = require('./utils/fileRename.js'),
	args = require('yargs').argv,
	timestamp = new Date().getTime(),
    fs = require('fs'),
    recursive = require('recursive-readdir'),
	runSequence = require('run-sequence');
	
/**
 * Cart Gulp Tasks
 * 
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @namespace Gulp
 * 
 * @since 2.0.0
 * @version 2.0.0
 */
module.exports = function (config) {
    /**
     * Cart Clean - gulp task that triggers clean task that will delete all files from "./dest/cart/" folder.
     * @memberof Gulp
     * @method cart:clean
     * @example <caption>gulp cart:clean</caption>
     */
	gulp.task('cart:clean', require('./tasks/clean')(
		[config.paths.dest.cart]
	));

    /**
     * JS Lint - gulp task that will check all "*.js" files from "config.paths.cart.custom.scripts" for js common mistakes.
     * @memberof Gulp
     * @method cart:jslint
     * @example <caption>gulp cart:jslint</caption>
     */
	gulp.task('cart:jslint', require('./tasks/jslint')(
		[config.paths.cart.custom.scripts + '/**/*']
	));
	
    /**
     * CSS Lint - gulp task that will check all "*.css" files from "config.paths.cart.custom.styles" for common mistakes.
     * @memberof Gulp
     * @method cart:csslint
     * @example <caption>gulp cart:csslint</caption>
     */
	gulp.task('cart:csslint', require('./tasks/csslint')(
		[config.paths.cart.custom.styles + '/**/*']
	));

    /**
     * AvaCart CSS - gulp task that will pre-process all the sources "*.less" from "config.paths.cart.styles.avaCart", and will deploy the ava_cart.css in "dest/cart".
     * @memberof Gulp
     * @method cart:styles
     * @example <caption>gulp cart:styles</caption>
     */
	gulp.task('cart:styles', require('./tasks/style')(
		fileRename('ava_cart', ''), 
		config.paths.cart.styles.avaCart, 
		config.paths.dest.cart,
		config.autoPrefixerBrowsers, 
		config.project.env === 'DEV'
	));
	
    /**
     * AvaCart JS - gulp task that will pre-process all the sources "*.js" from "[config.paths.cart.scripts.avaCart.framework,
         config.paths.cart.scripts.avaCart.pageStructure,
		 config.paths.cart.scripts.avaCart.layouts,
		 config.paths.cart.scripts.avaCart.widgets]", and will deploy the ava_cart.js in "dest/cart".
     * @memberof Gulp
     * @method cart:ava-cart
     * @example <caption>gulp cart:ava-cart</caption>
     */
	gulp.task('cart:ava-cart', require('./tasks/script')(
		'ava_cart.js', 
		[config.paths.cart.scripts.avaCart.framework,
         config.paths.cart.scripts.avaCart.pageStructure,
		 config.paths.cart.scripts.avaCart.layouts,
		 config.paths.cart.scripts.avaCart.widgets],
		(config.paths.dest.cart + '/ava-cart/'), 
		false,
		false
	));
	
    /**
     * AvaCart Init JS - gulp task that will pre-process all the sources "*.js" from "config.paths.cart.scripts.avaCartInit", and will deploy the ava_cart_init.js in "dest/cart".
     * @memberof Gulp
     * @method cart:ava-cart-init
     * @example <caption>gulp cart:ava-cart-init</caption>
     */
	gulp.task('cart:ava-cart-init', require('./tasks/script')(
		fileRename('ava_cart_init', '.js'), 
		config.paths.cart.scripts.avaCartInit,
		config.paths.dest.cart, 
		config.project.env === 'DEV',
		config.project.env === 'DEV'
	));
	
	/**
     * AvaCart Scripts JS - gulp task that will pre-process all the sources "*.js" from "config.paths.cart.scripts.avaCartScripts", and will deploy the ava_cart_scripts.js in "dest/cart".
     * @memberof Gulp
     * @method cart:ava-cart-scripts
     * @example <caption>gulp cart:ava-cart-scripts</caption>
     */
	gulp.task('cart:ava-cart-scripts', require('./tasks/script')(
		fileRename('ava_cart_scripts', '.js'), 
		config.paths.cart.scripts.avaCartScripts,
		config.paths.dest.cart, 
		config.project.env === 'DEV',
		config.project.env === 'DEV'
	));
	
	/**
     * AvaCart Translations JS - gulp task that will pre-process all the sources "*.js" from "config.paths.cart.scripts.avaCartTranslations", and will deploy the ava_cart_translations.js in "dest/cart".
     * @memberof Gulp
     * @method cart:ava-cart-translations
     * @example <caption>gulp cart:ava-cart-translations</caption>
     */
	gulp.task('cart:ava-cart-translations', require('./tasks/script')(
		fileRename('ava_cart_translations', '.js'),
		config.paths.cart.scripts.avaCartTranslations,
		config.paths.dest.cart, 
		config.project.env === 'DEV',
		config.project.env === 'DEV'
	));
	
    /**
     * Cart Scripts JS - that will execute sequencial "cart:ava-cart-scripts" , "cart:ava-cart-init" and "cart:ava-cart-translations" tasks.
     * @memberof Gulp
     * @method cart:scripts
     * @example <caption>gulp cart:scripts</caption>
     */
	gulp.task('cart:scripts', function(){
		return runSequence('cart:ava-cart-scripts', 'cart:ava-cart-init', 'cart:ava-cart-translations');
	});
    
    /**
     * Cart Concatenated Scripts JS - gulp task that will pre-process all the sources "*.js" from "config.paths.cart.dest", and will deploy the ava_cart_scripts_concatenated.js in "dest/cart".
     * @memberof Gulp
     * @method cart:scripts:concatenated
     * @example <caption>gulp cart:scripts:concatenated</caption>
     */
	gulp.task('cart:scripts:concatenated', require('./tasks/script')(
		fileRename('ava_cart_scripts_concatenated', '.js'), 
		[
			config.paths.dest.cart + '/' + fileRename('ava_cart_translations', '.js'),
			config.paths.dest.cart + '/' + fileRename('ava_cart_scripts', '.js'),
			config.paths.dest.cart + '/' + fileRename('ava_cart_templates', '.js'),
			config.paths.dest.cart + '/' + fileRename('ava_cart_init', '.js')
        ],
		config.paths.dest.cart, 
		config.project.env === 'DEV',
		config.project.env === 'DEV'
	));
    
     /**
     * AvaCart Templates - gulp task that will pre-process all the sources "*.html" from "config.paths.cart.templates", and will deploy the ava_cart_templates.js in "dest/cart".
     * @memberof Gulp
     * @method cart:templates
     * @example <caption>gulp cart:templates</caption>
     */
	gulp.task('cart:templates', require('./utils/html2js.js')(
		config.paths.cart.templates,
		config.paths.dest.cart + '/' + fileRename('ava_cart_templates', '.js')
	));
    
	/**
     * Cart Init Backup - gulp task that will create a backup of the js, html and less files that are used in the boilerplate and will create a backup in "./tmp/backup/". All backup files will be stored in a timestamped folder.
     * @memberof Gulp
     * @method cart:init:backup
     * @example <caption>gulp cart:init:backup</caption>
     */
	gulp.task('cart:init:backup', require('./tasks/copy')(
		["./scripts/cart/**/*", "./templates/cart/**/*" , "./styles/cart/**/*"],	
		config.paths.tmp + '/cart/' + timestamp
	));
	
   /**
     * Cart Init Clean - gulp task that triggers clean task that will delete all js, html and less files that are used in the boilerplate.
     * @memberof Gulp
     * @method cart:init:clean
     * @example <caption>gulp cart:init:clean</caption>
     */
	gulp.task('cart:init:clean', require('./tasks/clean')(
		["./scripts/cart/**/*", "./templates/cart/**/*" , "./styles/cart/**/*"]
	));
	
    
    var layout = args.layout ? args.layout : config.project.layout;
   /**
     * Cart Init Styles - gulp task that will copy all the ".less" resources from the layout configuration into the boilerplate.
     * @memberof Gulp
     * @method cart:init:styles
     * @example <caption>gulp cart:init:styles --layout 1</caption>
     * @param {string} --layout 
     */
    gulp.task('cart:init:styles', require('./tasks/copy')(
		[config.paths.cart.configurations + 'layout-'+ layout + '/**/*.less'],	
		config.paths.cart.custom.styles 
	));
    
	/**
     * Cart Init Scripts - gulp task that will copy all the ".js" resources from the layout configuration into the boilerplate.
     * @memberof Gulp
     * @method cart:init:scripts
     * @example <caption>gulp cart:init:scripts --layout 1</caption>
     * @param {string} --layout 
     */
	gulp.task('cart:init:scripts', require('./tasks/copy')(
		[config.paths.cart.configurations + 'layout-'+ layout + '/**/*.js'],	
		config.paths.cart.custom.scripts
	));
    
	/**
     * Cart Init - gulp task that executes all dependencies and will initialize the layout into the boilerplate.
     * @memberof Gulp
     * @method cart:init
     * @example <caption>gulp cart:init --layout 1</caption>
     * @param {string} --layout 
     */
	gulp.task('cart:init', function(){
		return runSequence('cart:init:backup', 'cart:init:clean', 'cart:init:styles', 'cart:init:scripts', 'cart');
	});
    
	/**
     * Cart Watch - gulp task that executes all dependencies and will watch the resources fron boilerplate, automatically generating granular files when this resources are modified.
     * @memberof Gulp
     * @method cart:watch
     * @example <caption>gulp cart:watch</caption>
     */
	gulp.task('cart:watch', ['cart:watch:styles','cart:watch:scripts', 'cart:watch:templates']);
    
    // Cart Styles Watch Task
	gulp.task('cart:watch:styles', require('./tasks/watch')(
		config.listenPort, 
		[{ paths: [config.paths.cart.custom.styles + '/**/*'], 
                    tasks: ['cart:styles'] }]
	));
    
     // Cart Styles Watch Task
	gulp.task('cart:watch:scripts', require('./tasks/watch')(
		config.listenPort, 
		[{ paths: [config.paths.cart.custom.scripts + '/**/*'], 
                    tasks: ['cart:scripts'] }]
	));
    
    // Cart Styles Watch Task
	gulp.task('cart:watch:templates', require('./tasks/watch')(
		config.listenPort, 
		[{ paths: [config.paths.cart.custom.templates + '/**/*'], 
                    tasks: ['cart:templates'] }]
	));
    
    gulp.task('cart:widgets:configs', recursive('./bower_components/avaframework/cart/scripts/widgets/', ['*.html'], function (err, files) {
        var avaLog = function() {};
        eval(fs.readFileSync('./bower_components/avaframework/cart/scripts/ava_cart.js') + '');
        files.forEach(function (file) {
            eval(fs.readFileSync(file) + '');
        });
        var output = {};
        for (var i in AvaCart.Widgets) {
            output[i] = AvaCart.Widgets[i].settings;
        }
        // write template variables to js file
        fs.writeFile('./bower_components/avaframework/cart/scripts/ava_cart_widgets_config.json', JSON.stringify(output), function (err) {
            if (err) {
                return console.log(err);
            }
            console.log('The file was saved!');
        });
    }));
	 
    /**
     * Cart Default - gulp task that executes all dependencies for generating the cart.
     * @memberof Gulp
     * @method cart
     * @example <caption>gulp cart</caption>
     */
	gulp.task('cart', function(){
		return runSequence('cart:clean', /*'cart:csslint',*/ 'cart:jslint', 'cart:styles', 'cart:scripts', 'cart:templates');
	});
};