// Task Dependencies
var htmlToJs = require('html-to-js'),
	fs = require('fs'),
	read = fs.readFileSync;

/**
* Create Js variables from HTML templates.
 * @memberof Gulp
 * @method html2js
* @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
* @version 1.0.0
* @since   1.0.0
* @example
	gulp.task('taskName', require('./utils/html2js.js')(
		src,
		dest
	));
*/
module.exports = function (tpls, dest) {
	return function() {
		var source = [];
		for (var i in tpls) { // iterate objects in array
			for (var j in tpls[i]) {
				
				var html = read(tpls[i][j], 'utf8'); // read file from path
				var js = htmlToJs(html);
				// push templates as variables in source array
				source.push(js.replace('module.exports', j + '.template'));
			}
		}
		// write template variables to js file
		fs.writeFile(dest, source.join(''), function (err) {
			if (err) {
				return console.log(err);
			}
			console.log('The file was saved!');
		});
	}
}