var config = require('../../avaframework.json'),
    args = require('yargs').argv;

/**
* File rename using AvaFramework standard.
 * @memberof Gulp
 * @method fileRename
 * @param {string} filename
 * @param {ext} ext
 * @example 
 var fileRename = require('./utils/fileRename.js');
 fileRename('ava_cart', '.js')

 */
module.exports = function (filename, ext) {
    if (config.project.env === 'dev') {
        return (filename + ext).toLowerCase();
    } else {
        return [config.project.vendorCode, config.project.cartTemplateID, filename, config.project.env].join('_').toLowerCase() + ext;
    }
}