// Dependencies
var del = require('del');

/**
 * Clean Task
 * @memberof Gulp
 * @method clean
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * @since 1.0.0
 * @version 1.0.0
 *
 * @param {array} paths
 * @example 	
    gulp.task('taskName', require('./tasks/clean')(
        [src]
    ));
 */
module.exports = function(paths) {
    return function(cb) {
        return del(paths, cb);
    }
};