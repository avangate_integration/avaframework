// Dependencies
var gulp = require('gulp'),
	recess = require('gulp-recess'),
    notify = require('gulp-notify'),
    recessrc = require('../config/.recessrc.json');

/**
 * JS Hint
 * 
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * 
 * @memberof Gulp
 * @method csslint
 * 
 * @since 1.0.0
 * @version 1.0.0
 *
 * @param {array} paths
 * @example 	
    gulp.task('taskName', require('./tasks/csslint')(
        [src]
    ));
 */
module.exports = function(paths) {
    return function (cb) {
        return gulp.src(paths)
            .pipe(recess(recessrc))
            .pipe(recess.reporter())
            .pipe(notify({
                title: 'CSS Lint',
                message: 'CSS Lint Passed!',
            }))
    }
};