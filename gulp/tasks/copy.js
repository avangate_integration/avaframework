// Dependencies
var gulp = require('gulp');
 
/**
 * Copy files from src to dest.
 * 
 * @memberof Gulp
 * @method copy
 * 
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * 
 * @since  1.0.0
 * @version  1.0.0
 *
 * @param {Array} src
 * @param {String} dest
 * 
 * @example 
	gulp.task('taskname', require('./tasks/copy')(
		[src, src2],	
		destination
	));
 */
module.exports = function (src, dest) {
    return function (cb) {
        return gulp.src(src)
                .pipe(gulp.dest(dest));
    }
};