// Task Dependencies
var gulp = require('gulp'),
    server = require('tiny-lr')();

/**
 * Watch for files change.
 * 
 * @memberof Gulp
 * @method watch
 * 
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * 
 * @memberof Gulp
 * @method watch
 * 
 * @since  1.0.0
 * @version  1.0.0
 *
 * @param  {string} listenPort 
 * @param  {array}  watchFiles
 * @example 
    // Cart Styles Watch Task
gulp.task('taskName', require('./tasks/watch')(
    listenPort, 
    [
        { paths: [src], 
          tasks: ['tasksThatShouldBeExecutedWhenFilesFromThisPathsChange'] 
         }
    ]
));
 */
module.exports = function(listenPort, watchFiles) {
    return function(cb) {
        server.listen(listenPort, function(err) {
            if (err) {
                return console.log(err)
            };
            for (var i in watchFiles) {
                return gulp.watch(watchFiles[i].paths, watchFiles[i].tasks);
            };
        });
    }
};