// Dependencies
var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    notify = require('gulp-notify'),
    jshintrc = require('../config/.jshintrc.json');

/**
 * JS Hint
 * 
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * 
 * @memberof Gulp
 * @method jslint
 * 
 * @since 1.0.0
 * @version 1.0.0
 *
 * @param {array} paths
 * @example 	
    gulp.task('taskName', require('./tasks/jslint')(
        [src]
    ));
 */
module.exports = function(paths) {
    return function (cb) {
        return gulp.src(paths)
            .pipe(jshint(jshintrc))
            .pipe(jshint.reporter('jshint-stylish', {
                verbose: true
            }))
            .pipe(jshint.reporter('fail'))
            .pipe(notify({
                title: 'JSHint',
                message: 'JSHint Passed!',
            }))
    }
};