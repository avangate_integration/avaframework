// Task Dependencies
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    gulpif = require('gulp-if'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    less = require('gulp-less'),
    minifyCss = require('gulp-minify-css'),
    sakugawa = require('gulp-sakugawa'),
    livereload = require('gulp-livereload'),
    server = require('tiny-lr')(),
    debug = require('gulp-debug');
    
/**
 * Compile and generate a concatenated style. 
 * 
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * 
 * @memberof Gulp
 * @method style
 * 
 * @since  1.0.0
 * @version  1.0.0
 * 
 * @param  {string}  fileName The filename that the new file should have.
 * @param  {array}  src Sources that will be compile and concatenated.                   
 * @param  {string}  dest             
 * @param  {array}  autoPrefixerBrowsers   
 * @param  {boolean}  enableSourceMaps   
  * @example 
 gulp.task('taskName', require('./tasks/style')(
    'filename.css', 
    src,
    dest, 
    autoPrefixerBrowsers,
    enableSourceMaps
));
 */
module.exports = function (fileName, src, dest, autoPrefixerBrowsers, enableSourceMaps) {
    return function (cb) {
        return gulp.src(src)
            .pipe(concat(fileName))
            // .pipe(gulpif(enableSourceMaps === true, sourcemaps.init()))
            .pipe(less({
                errLogToConsole: true
            }))
            .pipe(autoprefixer(autoPrefixerBrowsers))
            // This exports the entire CSS
            .pipe(gulp.dest(dest))
            // This will export split CSSs files / IE9 case
            .pipe(sakugawa({
                maxSelectors: 4000,
                mediaQueries: 'separate',
                suffix: '_'
            }))
            .pipe(minifyCss({
                keepSpecialComments: 1
            }))
            .pipe(livereload(server))
            // .pipe(gulpif(enableSourceMaps === true, sourcemaps.write()))
            .pipe(gulp.dest(dest));
    }
};