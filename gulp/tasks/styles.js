// Task Dependencies
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    gulpif = require('gulp-if'),
    autoprefixer= require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    less = require('gulp-less'),
    minifyCss = require('gulp-minify-css'),
    livereload = require('gulp-livereload'),
    server = require('tiny-lr')();
    
/**
 * Compile and generate a concatenated style. 
 * 
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * 
 * @since  1.0.0
 * @version  1.0.0
 *
 * @param  String, Array  src              
 * @param  String  dest             
 * @param  Array  autoPrefixerBrowsers   
 * @param  Bool  enableSourceMaps   
 */
module.exports = function (src, dest, autoPrefixerBrowsers, enableSourceMaps) {
	return function (cb) {
        return gulp.src(src)
                .pipe(gulpif(enableSourceMaps === true, sourcemaps.init()))
                .pipe(less({
                    errLogToConsole: true
                }))
                .pipe(autoprefixer(autoPrefixerBrowsers))
                .pipe(minifyCss({
                    keepSpecialComments: 1
                }))
                .pipe(livereload(server))
                .pipe(gulpif(enableSourceMaps === true, sourcemaps.write()))
                .pipe(gulp.dest(dest));
	}
};