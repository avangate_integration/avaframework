// Task Dependencies
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    gulpif = require('gulp-if'),
    sourcemaps = require('gulp-sourcemaps'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    livereload = require('gulp-livereload'),
    jshintrc = require('../config/.jshintrc.json'),
    server = require('tiny-lr')();

/**
 * Compile and generate a concatenated single file script from multiple or single sources. 
 * 
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * 
 * @memberof Gulp
 * @method script
 * 
 * @since  1.0.0
 * @version  1.0.0
 *
 * @param  {string}  fileName The filename that the new file should have.
 * @param  {array}  src Sources that will be compile and concatenated.             
 * @param  {string}  dest             
 * @param  {boolean}  enableSourceMaps 
 * @param  {boolean}  enableJsHint     
 * @example 
 gulp.task('taskName', require('./tasks/script')(
    'filename.js', 
    src,
    dest, 
    enableSourceMaps,
    enableJsHint
));
 */
module.exports = function (fileName, src, dest, enableSourceMaps, enableJsHint) {
	return function(cb) {
       return gulp.src(src)
        		.pipe(concat(fileName))
        		.pipe(gulpif(enableSourceMaps === true, sourcemaps.init()))
        		.pipe(gulpif(enableJsHint === true, jshint(jshintrc)))
        		.pipe(gulpif(enableJsHint === true, jshint.reporter('jshint-stylish')))
        		.pipe(gulpif(enableSourceMaps === true, sourcemaps.write()))
        		.pipe(gulp.dest(dest))
        		.pipe(rename({
        			suffix: '.min'
        		}))
        		.pipe(uglify())
        		.pipe(livereload(server))
        		.pipe(gulpif(enableSourceMaps === true, sourcemaps.write()))
        		.pipe(gulp.dest(dest))
    }
};