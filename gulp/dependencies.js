// Dependencies
var gulp = require('gulp'),
	runSequence = require('run-sequence');

/**
 * Legacy Gulp Tasks
 * 
 * @author Adrian Staniloiu (adrian.staniloiu@avangate.com)
 * 
 * @since 2.0.0
 * @version 2.0.0
 */

module.exports = function (config) {
	// Cart Dependencies Clean Task
	gulp.task('dependencies:cart:clean', require('./tasks/clean')(
		[config.paths.dest.cart + '/dependencies/']
	));
	
    /**
     * Cart Dependencies - gulp task that will pre-process all the sources from "config.paths.cart.scripts.dependencies", and will deploy the ava_cart_dependencies.js in "dest/cart/dependencies".
     * @memberof Gulp
     * @method dependencies:cart
     * @example <caption>gulp dependencies:cart</caption>
     */
	gulp.task('dependencies:cart', ['dependencies:cart:clean'], require('./tasks/script')(
		'ava_cart_dependencies.js',
		config.paths.cart.scripts.dependencies,
		config.paths.dest.cart + '/dependencies/',
		false,
		false
	));
    
    // Cart Dependencies Task
	gulp.task('dependencies', function(){
		return runSequence('dependencies:cart');
	});
	
};